import styled from "styled-components";

export const ContactsDropdownOuterWrapper = styled.div`
  height: calc(100vh - 100px);
  width: calc(100vw - 100px);
  background-color: #ffffff;
  display: flex;
  padding: 5px;

`

export const ContactsDropdownInnerWrapper = styled.div`
  height: 100%;
  width: 100%;
  border: 1px solid #d0d0d0;
  display: flex;

`

export const ContactsSidebarWrapper = styled.div`
  width: 350px;
  height: 100%;
  background-color: #f6f6f6;
  padding: 10px;
  display: flex;
  flex-direction: column;
`

export const ContactsFilterWrapper = styled.div`
  margin-top: 10px;
`


export const FilterHeaderWrapper = styled.div`
  width: 100%;
  height: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 10px;
  border: 1px solid #d7d7d7;
  background-color: #ffffff;
`

export const FilterHeaderText = styled.p`
  font-size: 14px;
  font-weight: bold;
  line-height: 1;
  margin: 0;
`

export const ContactFilterItemWrapper = styled.div`
  width: 100%;
  padding: 10px;
  border: 1px solid #d7d7d7;
  border-top: 0;
  background-color: #ffffff;
`

export const ContactsTableOuterWrapper = styled.div`
  width: calc(100% - 350px);
  height: 100%;
  padding: 10px 10px 10px 0;
  background-color: #f6f6f6;

`

export const ContactsTableInnerWrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: #ffffff;

`