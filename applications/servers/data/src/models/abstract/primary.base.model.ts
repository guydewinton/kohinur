import {RefModel} from "../metastructural/ref.model";
import {AbstractBaseModel} from "./abstract.base.model";
import {OneToOneRelation} from "../../library/model/app/decorators/prop/relation.prop.decorator";
import {PrimaryGeneratedUUIDField} from "../../library/model/app/decorators/prop/identifier.prop.decorator";


export abstract class PrimaryBaseModel extends AbstractBaseModel {

    @PrimaryGeneratedUUIDField()
    @OneToOneRelation(() => RefModel, ref => ref.id, {cascade: true})
    id: string;

}