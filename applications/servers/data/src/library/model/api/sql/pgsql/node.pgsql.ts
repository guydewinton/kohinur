export const plsqlNode = (tableName: string) => {
    return `

CREATE OR REPLACE FUNCTION node_handler() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            DELETE FROM node_model WHERE node_model.id = OLD.id;
        ELSIF (TG_OP = 'INSERT') THEN
        
              INSERT INTO node_model (name) VALUES (TG_TABLE_NAME) RETURNING id;
              NEW.id = id;
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER ${tableName}_insert
BEFORE INSERT ON ${tableName}
    FOR EACH ROW EXECUTE FUNCTION node_handler();


CREATE TRIGGER ${tableName}_update
AFTER UPDATE ON emp_view ${tableName}
    FOR EACH ROW EXECUTE FUNCTION node_handler();


CREATE TRIGGER ${tableName}_delete
AFTER DELETE ON ${tableName}
    FOR EACH ROW EXECUTE FUNCTION node_handler();
`

}

















// import { getManager } from 'typeorm';
//
// const entityManager = getManager();
// const someQuery = entityManager.query(`
//   SELECT
//     fw."X",
//     fw."Y",
//     ew.*
//   FROM "table1" as fw
//   JOIN "table2" as ew
//     ON fw."X" = $1 AND ew.id = fw."Y";
//   `, [param1]);


// `
// Would be super amazing if all could be handled from the node.
//
// relay style... literally the schema and postgres handling is relay compliant
//
//                 ###########################
//                 ######### GQL AST #########
//                 ###########################
//
//           "name": "node",
//           "type": {
//             "name": "Node",
//             "kind": "INTERFACE"
//           },
//
//           "args": [
//
//
//               {
//                   "name": "id",
//                   "type": {
//                     "kind": "NON_NULL",
//                     "ofType": {
//                       "name": "ID",
//                       "kind": "SCALAR"
//                     }
//                   }
//                 }
//
//                 ###########################
//                 ###########################
//                 ###########################
//
//         "id": "uuid",
//         "type": {
//         "name": "DB_TableName/ModelName",
//         },
//
//                 ###########################
//                 ######!!!!!!!!!!!!!!!######
//                 ###########################
//
// id/local/remote
//
// hidden fields on the node table only pg can see
//
// type... all handled programmatically... will need to also watch for table renaming
//
// or excluded from external hasura / orm-graph resolvers
//
// functions routed according to typeorm inputs into naming... (class extension/decorators)
// pghandler create/delete function on any primary typeorm table
// pghandler update function on any primary typeorm table
//
// dynamic SQL
//
// in the entity manager... ideal would be on each table migration... SQL is injected - vault? a bit much
// next would be once on launch//probably also extending class extension/decorators
// if present cleanup =>  redeploy. best... most up to date... and easiest, most exposed... can be a check... toggle
// default off ... does file check if file present can read
//
//
// id uuid
// name table name
// (type FK)
//
//
// polymorphs have their own logic in pg
//
// but then how will typeorm know... I guess multiple queries and a router to the correct resolver
// first node, get id and name >> then second lookup for id on that table...
// that is node resolution.
//
//
// RELAY GQL:
//
// <<<<<>>>>>
// :::SPEC:::
// <<<<<>>>>>
//
// interface Node {
//   id: ID!
// }
//
// type Faction implements Node {
//   id: ID!
//   name: String
//   ships: ShipConnection
// }
//
// type Ship implements Node {
//   id: ID!
//   name: String
// }
//
// type ShipConnection {
//   edges: [ShipEdge]
//   pageInfo: PageInfo!
// }
//
// type ShipEdge {
//   cursor: String!
//   node: Ship
// }
//
// type PageInfo {
//   hasNextPage: Boolean!
//   hasPreviousPage: Boolean!
//   startCursor: String
//   endCursor: String
// }
//
// type Query {
//   rebels: Faction
//   empire: Faction
//   node(id: ID!): Node
// }
//
// <<<<<>>>>>>
// :::QUERY:::
// <<<<<>>>>>>
//
// query RebelsRefetchQuery {
//   node(id: "RmFjdGlvbjox") {
//     id
//     ... on Faction {
//       name
//     }
//   }
// }
//
// <<<<<<>>>>>>
// :::RETURN:::
// <<<<<<>>>>>>
//
// {
//   "node": {
//     "id": "RmFjdGlvbjox",
//     "name": "Alliance to Restore the Republic"
//   }
// }
//
// <<<<<<>>>>>>
//
//
//
//
//
// `

export const pgsqlNode_AuditingWithTransitionTables = `


CREATE TABLE emp (
    empname           text NOT NULL,
    salary            integer
);

CREATE TABLE emp_audit(
    operation         char(1)   NOT NULL,
    stamp             timestamp NOT NULL,
    userid            text      NOT NULL,
    empname           text      NOT NULL,
    salary integer
);

CREATE OR REPLACE FUNCTION process_emp_audit() RETURNS TRIGGER AS $emp_audit$
BEGIN
--
    -- Create rows in emp_audit to reflect the operations performed on emp,
    -- making use of the special variable TG_OP to work out the operation.
--
    IF (TG_OP = 'DELETE') THEN
INSERT INTO emp_audit
SELECT 'D', now(), user, o.* FROM old_table o;
ELSIF (TG_OP = 'UPDATE') THEN
INSERT INTO emp_audit
SELECT 'U', now(), user, n.* FROM new_table n;
ELSIF (TG_OP = 'INSERT') THEN
INSERT INTO emp_audit
SELECT 'I', now(), user, n.* FROM new_table n;
END IF;
RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$emp_audit$ LANGUAGE plpgsql;

CREATE TRIGGER emp_audit_ins
AFTER INSERT ON emp
REFERENCING NEW TABLE AS new_table
FOR EACH STATEMENT EXECUTE FUNCTION process_emp_audit();
CREATE TRIGGER emp_audit_upd
AFTER UPDATE ON emp
REFERENCING OLD TABLE AS old_table NEW TABLE AS new_table
FOR EACH STATEMENT EXECUTE FUNCTION process_emp_audit();
CREATE TRIGGER emp_audit_del
AFTER DELETE ON emp
REFERENCING OLD TABLE AS old_table
FOR EACH STATEMENT EXECUTE FUNCTION process_emp_audit();
    
`



















export const pqsqlNode_TriggerFunctionForMaintainingSummaryTable = `
CREATE TABLE time_dimension (
    time_key                    integer NOT NULL,
    day_of_week                 integer NOT NULL,
    day_of_month                integer NOT NULL,
    month                       integer NOT NULL,
    quarter                     integer NOT NULL,
    year                        integer NOT NULL
);
CREATE UNIQUE INDEX time_dimension_key ON time_dimension(time_key);

CREATE TABLE sales_fact (
    time_key                    integer NOT NULL,
    product_key                 integer NOT NULL,
    store_key                   integer NOT NULL,
    amount_sold                 numeric(12,2) NOT NULL,
    units_sold                  integer NOT NULL,
    amount_cost                 numeric(12,2) NOT NULL
);
CREATE INDEX sales_fact_time ON sales_fact(time_key);

--
-- Summary table - sales by time.
--
CREATE TABLE sales_summary_bytime (
    time_key                    integer NOT NULL,
    amount_sold                 numeric(15,2) NOT NULL,
    units_sold                  numeric(12) NOT NULL,
    amount_cost                 numeric(15,2) NOT NULL
);
CREATE UNIQUE INDEX sales_summary_bytime_key ON sales_summary_bytime(time_key);

--
-- Function and trigger to amend summarized column(s) on UPDATE, INSERT, DELETE.
--
CREATE OR REPLACE FUNCTION maint_sales_summary_bytime() RETURNS TRIGGER
AS $maint_sales_summary_bytime$
    DECLARE
        delta_time_key          integer;
        delta_amount_sold       numeric(15,2);
        delta_units_sold        numeric(12);
        delta_amount_cost       numeric(15,2);
    BEGIN

        -- Work out the increment/decrement amount(s).
        IF (TG_OP = 'DELETE') THEN

            delta_time_key = OLD.time_key;
            delta_amount_sold = -1 * OLD.amount_sold;
            delta_units_sold = -1 * OLD.units_sold;
            delta_amount_cost = -1 * OLD.amount_cost;

        ELSIF (TG_OP = 'UPDATE') THEN

            -- forbid updates that change the time_key -
            -- (probably not too onerous, as DELETE + INSERT is how most
            -- changes will be made).
            IF ( OLD.time_key != NEW.time_key) THEN
                RAISE EXCEPTION 'Update of time_key : % -> % not allowed',
                                                      OLD.time_key, NEW.time_key;
            END IF;

            delta_time_key = OLD.time_key;
            delta_amount_sold = NEW.amount_sold - OLD.amount_sold;
            delta_units_sold = NEW.units_sold - OLD.units_sold;
            delta_amount_cost = NEW.amount_cost - OLD.amount_cost;

        ELSIF (TG_OP = 'INSERT') THEN

            delta_time_key = NEW.time_key;
            delta_amount_sold = NEW.amount_sold;
            delta_units_sold = NEW.units_sold;
            delta_amount_cost = NEW.amount_cost;

        END IF;


        -- Insert or update the summary row with the new values.
        <<insert_update>>
        LOOP
            UPDATE sales_summary_bytime
                SET amount_sold = amount_sold + delta_amount_sold,
                    units_sold = units_sold + delta_units_sold,
                    amount_cost = amount_cost + delta_amount_cost
                WHERE time_key = delta_time_key;

            EXIT insert_update WHEN found;

            BEGIN
                INSERT INTO sales_summary_bytime (
                            time_key,
                            amount_sold,
                            units_sold,
                            amount_cost)
                    VALUES (
                            delta_time_key,
                            delta_amount_sold,
                            delta_units_sold,
                            delta_amount_cost
                           );

                EXIT insert_update;

            EXCEPTION
                WHEN UNIQUE_VIOLATION THEN
                    -- do nothing
            END;
        END LOOP insert_update;

        RETURN NULL;

    END;
$maint_sales_summary_bytime$ LANGUAGE plpgsql;

CREATE TRIGGER maint_sales_summary_bytime
AFTER INSERT OR UPDATE OR DELETE ON sales_fact
    FOR EACH ROW EXECUTE FUNCTION maint_sales_summary_bytime();

INSERT INTO sales_fact VALUES(1,1,1,10,3,15);
INSERT INTO sales_fact VALUES(1,2,1,20,5,35);
INSERT INTO sales_fact VALUES(2,2,1,40,15,135);
INSERT INTO sales_fact VALUES(2,3,1,10,1,13);
SELECT * FROM sales_summary_bytime;
DELETE FROM sales_fact WHERE product_key = 1;
SELECT * FROM sales_summary_bytime;
UPDATE sales_fact SET units_sold = units_sold * 2;
SELECT * FROM sales_summary_bytime;

`




















export const plsqlNode_ViewTriggerFunctionForAuditing = `

CREATE OR REPLACE FUNCTION node_handler() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            DELETE FROM node_model WHERE node_model.id = OLD.id;
        ELSIF (TG_OP = 'INSERT') THEN
        
              INSERT INTO node_model (name) VALUES (TG_TABLE_NAME) RETURNING id;
              NEW.id = id;
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER ${(prop: string) => prop}_insert
BEFORE INSERT ON ${(prop: string) => prop}
    FOR EACH ROW EXECUTE FUNCTION node_handler();


CREATE TRIGGER ${(prop: string) => prop}_update
AFTER UPDATE ON emp_view ${(prop: string) => prop}
    FOR EACH ROW EXECUTE FUNCTION node_handler();


CREATE TRIGGER ${(prop: string) => prop}_delete
AFTER DELETE ON ${(prop: string) => prop}
    FOR EACH ROW EXECUTE FUNCTION node_handler();
`

// will need to create one of these for each model