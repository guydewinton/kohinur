import "dotenv/config";
import "reflect-metadata";
import express, {Router} from 'express';
import {createConnection} from "typeorm";
import cookieParser from 'cookie-parser'

import authRouter from "./routes/auth/auth.route";
import accessRouter from "./routes/token/access.route";
import refreshRouter from "./routes/token/refresh.route";
import cors from "cors";
import userRouter from "./routes/user/user.http.route";


(async () => {

    try {
        await createConnection();
    } catch (err){
        console.log(err)
    }

    const app = express();

    const allowedOrigins = [
        'https://auth.mullumfoodcoop.com',
        'https://login.mullumfoodcoop.com',
        'https://vault.mullumfoodcoop.com',
        'https://shop.mullumfoodcoop.com',
        'https://dev.mullumfoodcoop.com',
    ]
    const corsOptions = {
        origin: function (origin: any, callback: any) {
            console.log(origin)
            if (!origin || allowedOrigins.indexOf(origin) !== -1) {
                callback(null, true)
            } else {
                callback(new Error('Not allowed by CORS'))
            }
        },
        credentials: true,
    }

    app.use(cors(corsOptions));

    app.use(cookieParser());
    app.use(express.json());

    const rootRouter = Router();

    rootRouter.use('/auth', authRouter);
    rootRouter.use('/token/access', accessRouter);
    rootRouter.use('/token/refresh', refreshRouter);
    rootRouter.use('/user', userRouter);

    app.use('/v1', rootRouter)

    app.listen(process.env.HTTP_SERVER_PORT, () => {
        console.log(`Auth HTTP Server started... listening on port ${process.env.HTTP_SERVER_PORT}.`);
    });

})();
