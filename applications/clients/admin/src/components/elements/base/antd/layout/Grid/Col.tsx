import styled from "styled-components";
import {Col} from "antd/es";
import "antd/es/col/style"

const StyledCol = styled(Col)`
`

export const ColWidget = StyledCol

export type { ColProps, ColSize } from 'antd/es/col'