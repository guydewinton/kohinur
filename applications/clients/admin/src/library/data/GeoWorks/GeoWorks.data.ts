import {CheckboxOptions} from "../../../components/elements/widgets/checkboxes/Checkbox.type";



export const masterFilterToggle: CheckboxOptions[][] = [
    [
        {
            value: 'access',
            label: 'Filter By Job',
            checked: false,
            disabled: false,
        },
        {
            value: 'waterDamage',
            label: 'Filter By Status',
            checked: false,
            disabled: false,
        },
    ],
]

export const statusCheckboxGrid: CheckboxOptions[][] = [
    [
        // {
        //     value: 'unassigned',
        //     label: 'Unassigned',
        //     checked: true,
        //     disabled: true,
        // },
        {
            value: 'pending',
            label: 'Pending',
            checked: true,
            disabled: true,
        },
        // ], [
        //     {
        //         value: 'deferred',
        //         label: 'Deferred',
        //         checked: true,
        //         disabled: true,
        //     },
        {
            value: 'complete',
            label: 'Complete',
            checked: true,
            disabled: true,
        },

    ]
]

export const jobCheckboxGrid: CheckboxOptions[][] = [
    [
        {
            value: 'access',
            label: 'Access',
            checked: true,
            disabled: true,
        },
        {
            value: 'waterDamage',
            label: 'Water Damage',
            checked: true,
            disabled: true,
        },
    ],
    [
        {
            value: 'power',
            label: 'Power',
            checked: true,
            disabled: true,
        },

        {
            value: 'plumbing',
            label: 'Plumbing',
            checked: true,
            disabled: true,
        },
    ],
    [
        {
            value: 'mould',
            label: 'Mould',
            checked: true,
            disabled: true,
        },
        {
            value: 'rubbish',
            label: 'Rubbish',
            checked: true,
            disabled: true,
        },
    ],
    [
        {
            value: 'gas',
            label: 'Gas',
            checked: true,
            disabled: true,
        },
        {
            value: 'waterSupply',
            label: 'Water Supply',
            checked: true,
            disabled: true,
        },
    ],
    [
        {
            value: 'landSlippage',
            label: 'Land Slippage',
            checked: true,
            disabled: true,
        },
        {
            value: 'buildingIntegrity',
            label: 'Building Integrity',
            checked: true,
            disabled: true,
        },
    ],
    [
        {
            value: 'councilBins',
            label: 'Council Bins Lost',
            checked: true,
            disabled: true,
        },
        {
            value: 'additional',
            label: 'Additional',
            checked: true,
            disabled: true,
        },

    ]
]
