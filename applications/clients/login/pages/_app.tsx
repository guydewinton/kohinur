import '../styles/globals.css'

import { AppProps } from 'next/app'
import styled from "styled-components";

const AppWrapper = styled.div`
  height: 100%;
  width: 100%;
`

function App({ Component, pageProps }: AppProps) {
  return (
      <AppWrapper>
            <Component {...pageProps} />
      </AppWrapper>
  )
}

export default App