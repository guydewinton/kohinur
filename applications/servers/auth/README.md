# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command

## Endpoints:

### `/auth`

- `/authenticate-user`

    - Takes:
      - username
      - password

    - Returns:
      - refresh-token cookie
      - access-token header
      - status code

### `/bearer`

- `/validate-access-token`

    - Takes:
      - access-token header
    - Returns:
      - status code or redirect (`../refresh/validate-update-refresh-token`)

- `/new-access-secret`

    - Takes: 
      - none
    - Returns: 
      - none

### `/refresh`

- `/validate-update-refresh-token`

  - Takes:
    - refresh-token cookie
  - Returns:
    - access-token header
    - status code

- `/revoke-user-refresh-token`

  - Takes:
    - user ID
  - Returns: 
    - none

- `/revoke-all-refresh-tokens`

  - Takes: 
    - none
  - Returns: 
    - none


### `/system`

- `/reset-all-secrets`

  - Takes: 
    - none
  - Returns: 
    - none


### `/user`

- `/get-users` [dev only]
- `/create-user`
- `/update-user`
- `/delete-user`
- `/send-reset-password-link`
- `/reset-password`