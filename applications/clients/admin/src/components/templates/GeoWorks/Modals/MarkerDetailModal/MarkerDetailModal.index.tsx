import React, {
    ChangeEvent,
    useContext,
    useEffect,
    useLayoutEffect,
    useRef,
    useState
} from "react";
import useWindowSize from "../../../../../library/hooks/useWindowSize";
import {
    MapDraggableMarker
} from "../../../../elements/widgets/map/MapDraggableMarker/MapDraggableMarker.index";
import {DivIcon, LatLngExpression} from "leaflet";
import ReactDOMServer from "react-dom/server";
import {UserCircleIcon} from "../../../../elements/icons/svg/UserCircle/UserCircle";
import {TableWidget} from "../../../../elements/base/antd/display/Table/Table";
import {GeoWorksMarkerDict} from "../../../../../type/data/module/GeoWorks/marker.type";
import {
    StyledButtonSvgWrapper,
    StyledDescriptionsTitle, StyledDescriptionsTitleNotes,
    StyledIconButton,
    StyledJobToolBarWrapper,
    StyledModalDataWrapper,
    StyledModalInnerWrapper,
    StyledModalJobDataTableInnerWrapper,
    StyledModalJobDataTableWrapper,
    StyledModalJobDataWrapper,
    StyledModalMapWrapper,
    StyledModalMarkerDataLeftInnerWrapper,
    StyledModalMarkerDataLeftWrapper,
    StyledModalMarkerDataRightWrapper,
    StyledModalMarkerDataWrapper,
    StyledTextAreaNotes
} from "./MarkerDetailModal.style";
import {HousePin} from "../../../../elements/widgets/pins/HousePin/HousePin.index";
import {DataObject} from "../../../../../type/data/common/object.type";
import {JobsDetail} from "../../../../../type/data/module/GeoWorks/action.type";
import JobEditDialog from "../../Dialogs/JobEditDialog/JobEditDialog.index";
import {useAppDispatch} from "../../../../../store/hooks";
import {deleteMarker, updateMarker} from "../../../../../store/features/GeoWorks/slices/markers";
import {ExclamationCircleOutlinedIcon} from "../../../../elements/base/antd/general/Icon/Icon";
import { ModalWidget } from "../../../../elements/base/antd/window/Modal/Modal";
import { ButtonWidget } from "../../../../elements/base/antd/general/Button/Button";
import { FormWidget } from "../../../../elements/base/antd/entry/Form/Form";
import { InputWidget } from "../../../../elements/base/antd/entry/Input/Input";
import { ColWidget } from "../../../../elements/base/antd/layout/Grid/Col";

interface ModalPaneProps {
    onSave: (markerData: GeoWorksMarkerDict) => void;
    onCancel: () => void;
    mapMarker: GeoWorksMarkerDict;
    onDelete: (id: string) => void
}

export const MarkerDetailModal: React.FC<ModalPaneProps> = ({
            onSave,
            onCancel,
            mapMarker,
            onDelete,
        }) => {

    const [tableHeight, setTableHeight] = useState(0)
    const [markerLabelValue, setMarkerLabelValue] = useState<string>(mapMarker.label)
    const [markerNoteValue, setMarkerNoteValue] = useState<string>(mapMarker.note)
    const tableWrapperRef = useRef<HTMLDivElement>(null)
    const [markerPosition, setMarkerPosition] = useState<LatLngExpression>(mapMarker.position)
    const [markerJobs, setMarkerJobs] = useState<JobsDetail[]>(mapMarker.jobs)
    const [selectedJob, setSelectedJob] = useState<JobsDetail | null>(null)
    const [showEditJobModal, setShowEditJobModal] = useState<Boolean>(false)
    const [showNewJobModal, setShowNewJobModal] = useState<Boolean>(false)
    const [completedJobsArray, setCompletedJobsArray] = useState<string[]>([])
    const [toggleShowAllJob, setToggleShowAllJobs] = useState<boolean>(true)
    const [displayJobs, setDisplayJobs] = useState<JobsDetail[]>(mapMarker.jobs)

    useEffect(() => {
        if (selectedJob !== null) {
            setShowEditJobModal(true)
        }
    }, [selectedJob])

    useEffect(() => {
        setMarkerLabelValue(mapMarker.label)
    }, [mapMarker])

    useEffect(() => {
        let completedJobs: JobsDetail[]
        if (toggleShowAllJob) {
            completedJobs = [...markerJobs]
        } else {
            completedJobs = markerJobs.filter((job, i) => {
                return job.status !== 'complete'
            })
        }
        setDisplayJobs(completedJobs)
    }, [markerJobs, toggleShowAllJob])

    useEffect(() => {
        const completedJobs: string[] = []
        displayJobs.forEach((job, i) => {
            if (job.status === 'complete') {
                completedJobs.push(job.id)
            }
        })
        setCompletedJobsArray(completedJobs)
    }, [displayJobs])

    useLayoutEffect(() => {
        if (tableWrapperRef.current) {
            setTableHeight(tableWrapperRef.current.clientHeight - 89)
        }
    }, [])

    const {height, width} = useWindowSize()

    const handleRowSelectOne = (record: DataObject, selected: boolean) => {
        setMarkerJobs(prev => {
            return prev.map((job) => {
                if (job.id === record.id) {
                    return {...job, status: selected ? 'complete' : 'pending'}
                } else {
                    return job
                }
            })
        })
    }

    const handleRowSelectAll = (selected: boolean) => {
        setMarkerJobs(prev => {
            return prev.map((job) => {
                return {...job, status: selected ? 'complete' : 'pending'}
            })
        })
    }

    const mapMarkerCallback = (): DivIcon => {
        return new DivIcon({html: ReactDOMServer.renderToString(<HousePin/>)})
    }

    const handleMarkerLabelChange = (event: ChangeEvent<HTMLInputElement>) => {
        setMarkerLabelValue(event.target.value)
    }

    const handleMarkerNoteChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
        setMarkerNoteValue(event.target.value)
    }

    const handleSave = () => {
        onSave({
            id: mapMarker.id,
            label: markerLabelValue,
            position: markerPosition,
            note: markerNoteValue,
            jobs: markerJobs,
            access: 'private',
            contact: {
                type: 'resident',
                alias: 'Bob',
                givenName: 'Bob',
                surname: 'Harris',
            }
        })
    }

    const handleDeleteJob = (jobId: string) => {
        setShowEditJobModal(false)
        setMarkerJobs(prev => {
            return prev.filter(job => {
                return jobId !== job.id
            })
        })
    }


    const handleDeleteMarker = (id: string) => {
        onDelete(id)
    }

    const handleDrag = (markerPosition: LatLngExpression) => {
        setMarkerPosition(markerPosition)
    }

    const confirmDeleteJob = (id: string) => {
        ModalWidget.confirm({
            title: 'Delete Job?',
            icon: <ExclamationCircleOutlinedIcon />,
            content: 'Are you sure you want to delete this job?',
            okText: 'Delete',
            cancelText: 'Cancel',
            onOk: () => handleDeleteJob(id)
        });
    };

    const confirmDeleteMarker = (id: string) => {
        ModalWidget.confirm({
            title: 'Delete Marker?',
            icon: <ExclamationCircleOutlinedIcon />,
            content: 'Are you sure you want to delete this marker?',
            okText: 'Delete',
            cancelText: 'Cancel',
            onOk: () => id && handleDeleteMarker(id)
        });
    };

    const handleToggleAddJob = () => {
        setSelectedJob(null)
        setShowNewJobModal(true)

    }

    const handleToggleEditJob = (job: JobsDetail) => {
        return {
            onClick: () => {
                setSelectedJob(job)
            }
        }
    }

    const jobTableColumnArray = [
        {
            title: 'Description',
            dataIndex: 'issue',
            key: 'id',
            width: '68%'
        },
        {
            title: 'Type',
            dataIndex: 'typeLabel',
            key: 'typeLabel',
            width: '20%',
        },
    ]

    const handleSaveNewJob = (job: JobsDetail) => {
        setSelectedJob(null)
        setShowNewJobModal(false)
        setMarkerJobs(prev => {
            const returnJobs = [...prev]
            returnJobs.push(job)
            return returnJobs
        })
    }

    const handleSaveEditJob = (job: JobsDetail) => {
        setSelectedJob(null)
        setShowEditJobModal(false)
        setMarkerJobs(prev => {
            return prev.map((jobIter) => {
                if (job.id === jobIter.id) {
                    return job
                } else {
                    return jobIter
                }
            })
        })
    }

    const handleCancelNewJob = () => {
        setSelectedJob(null)
        setShowNewJobModal(false)

    }

    const handleCancelEditJob = () => {
        setSelectedJob(null)
        setShowEditJobModal(false)
    }

    const handleToggleShowAllJob = () => {
        setToggleShowAllJobs(prev => {
            return !prev
        })
    }

    return (
        <ModalWidget
            title={mapMarker.label}
            centered
            visible={true}
            footer={[
                <ButtonWidget key="delete" type="primary" onClick={() => confirmDeleteMarker(mapMarker.id)} danger>
                    Delete
                </ButtonWidget>,
                <ButtonWidget key="cancel" onClick={onCancel}>
                    Cancel
                </ButtonWidget>,
                <ButtonWidget key="save" type="primary" onClick={handleSave} >
                    Save
                </ButtonWidget>,

            ]}
            onCancel={onCancel}
            width={width * .9}
        >
            <StyledModalInnerWrapper height={height} >
                <StyledModalMapWrapper>
                    <MapDraggableMarker
                        center={mapMarker.position}
                        markerIconCallback={mapMarkerCallback}
                        zoom={16}
                        dragCallback={handleDrag}
                        markerId={mapMarker.id}
                    />
                </StyledModalMapWrapper>
                <StyledModalDataWrapper>
                    <StyledModalMarkerDataWrapper>
                        <StyledModalMarkerDataLeftWrapper>
                            <StyledDescriptionsTitle>Marker Info</StyledDescriptionsTitle>
                            <StyledModalMarkerDataLeftInnerWrapper>
                                <FormWidget labelCol={{span: 5}} wrapperCol={{span: 20}}>

                                            <FormWidget.Item label={'Label'}>
                                                <InputWidget
                                                    value={markerLabelValue}
                                                    onChange={handleMarkerLabelChange}
                                                    onKeyPress={(event) => {
                                                        if (event.key === "Enter") {
                                                            handleSave()
                                                        }
                                                    }}
                                                />
                                            </FormWidget.Item>

                                            <FormWidget.Item label={'Contact'}>
                                                <StyledIconButton >
                                                    {mapMarker.contact.alias}
                                                    <StyledButtonSvgWrapper>
                                                        <UserCircleIcon />
                                                    </StyledButtonSvgWrapper>
                                                </StyledIconButton>
                                            </FormWidget.Item>

                                </FormWidget>
                            </StyledModalMarkerDataLeftInnerWrapper>
                        </StyledModalMarkerDataLeftWrapper>
                        <StyledModalMarkerDataRightWrapper>

                                <StyledDescriptionsTitleNotes>Notes</StyledDescriptionsTitleNotes>

                                <StyledTextAreaNotes placeholder={'Write notes here...'} value={markerNoteValue} onChange={handleMarkerNoteChange}/>

                        </StyledModalMarkerDataRightWrapper>
                    </StyledModalMarkerDataWrapper>
                    <StyledModalJobDataWrapper>
                        <StyledModalJobDataTableWrapper ref={tableWrapperRef}>
                            <StyledModalJobDataTableInnerWrapper>
                                <TableWidget
                                    dataSource={displayJobs}
                                    columns={jobTableColumnArray}
                                    scroll={{y: tableHeight}}
                                    onRow={(data, _index) => handleToggleEditJob(data as JobsDetail)}
                                    pagination={false}
                                    rowKey={'id'}
                                    rowSelection={{
                                        onSelect: (record, selected, selectedRows) => {
                                            handleRowSelectOne(record as DataObject, selected)
                                        },
                                        onSelectAll: (selected, selectedRows, changeRows) => {
                                            handleRowSelectAll(selected)
                                        },
                                        selectedRowKeys: completedJobsArray,
                                    }}
                                />
                            </StyledModalJobDataTableInnerWrapper>
                            <StyledJobToolBarWrapper>
                                <ColWidget flex={1}>
                                    <FormWidget.Item style={{margin: 0, display: 'flex', justifyContent: 'center'}}>
                                        <ButtonWidget style={{width: '50%', borderRadius: 0}}
                                            onClick={handleToggleShowAllJob}
                                        >
                                            {toggleShowAllJob ? 'Hide Completed Jobs' : 'Show All Jobs'}
                                        </ButtonWidget>
                                        <ButtonWidget style={{width: '50%', borderRadius: 0}}
                                                onClick={handleToggleAddJob}
                                        >
                                            Add Job
                                        </ButtonWidget>
                                    </FormWidget.Item>
                                </ColWidget>

                            </StyledJobToolBarWrapper>
                        </StyledModalJobDataTableWrapper>
                    </StyledModalJobDataWrapper>
                </StyledModalDataWrapper>
            </StyledModalInnerWrapper>
            {
                showNewJobModal && (
                    <JobEditDialog
                        job={null}
                        onSave={handleSaveNewJob}
                        onCancel={handleCancelNewJob}

                    />
                )
            }
            {
                showEditJobModal && (
                    <JobEditDialog
                        job={selectedJob}
                        onSave={handleSaveEditJob}
                        onCancel={handleCancelEditJob}
                        onDelete={confirmDeleteJob}
                    />
                )
            }
        </ModalWidget>
    )
}