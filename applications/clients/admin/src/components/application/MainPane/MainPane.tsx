import React from "react";
import styled from "styled-components";
import {GeoWorksModule} from "../../../application/modules/GeoWorks/GeoWorks.module";

const MainPaneWrapper = styled.div`
  height: 100%;
  width: 100%;
`

export const MainPane: React.FC = ({children}) => {


    return (
        <MainPaneWrapper>
            {children}
        </MainPaneWrapper>
    )
}