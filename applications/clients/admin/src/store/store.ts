import {configureStore} from "@reduxjs/toolkit";
import markerSlice from "./features/GeoWorks/slices/markers";

export const store = configureStore({
    reducer: {
        marker: markerSlice,
    }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch