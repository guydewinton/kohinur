import React, {ChangeEvent, Dispatch, SetStateAction, useContext, useEffect, useRef, useState} from 'react';
import {StyledButtonSvgWrapper, StyledIconButton} from "../../Modals/MarkerDetailModal/MarkerDetailModal.style";
import {UserCircleIcon} from "../../../../elements/icons/svg/UserCircle/UserCircle";
import {InputRef, InputWidget} from "../../../../elements/base/antd/entry/Input/Input"
import { ModalWidget } from '../../../../elements/base/antd/window/Modal/Modal';
import { FormWidget } from '../../../../elements/base/antd/entry/Form/Form';
import {LatLngExpression} from "leaflet";

export interface MarkerAddNewDialogProps {
    newMarkerPosition: LatLngExpression
    onSave: (label: string, position: LatLngExpression) => void
    onCancel: () => void
    visible: boolean
}

const MarkerAddNewDialog: React.FC<MarkerAddNewDialogProps> = ({
                                          newMarkerPosition,
                                          onSave,
                                          onCancel,
                                            visible
                                      }) => {

    const [confirmLoading, setConfirmLoading] = useState(false)
    const [markerLabelValue, setMarkerLabelValue] = useState<string>('')
    const [inputStatus, setInputStatus] = useState<'' | 'error'>('')
    const initInput = useRef<InputRef>(null)
    const pageLoaded = useRef<boolean>(false)

    useEffect(() => {
        if (!pageLoaded.current && initInput.current) {
            initInput.current.focus({
                cursor: 'start',
            })
            pageLoaded.current = true
        }
    })

    const handleOk = () => {
        if (markerLabelValue) {
            setConfirmLoading(true)
            setTimeout(() => {
                setConfirmLoading(false)
                onSave(markerLabelValue, newMarkerPosition)
            }, 300)
        } else {
            setInputStatus('error')
        }
    };

    const handleCancel = () => {
        onCancel()
        setMarkerLabelValue('')
    };

    const handleMarkerLabelChange = (event: ChangeEvent<HTMLInputElement>) => {
        setMarkerLabelValue(event.target.value)
        inputStatus === 'error' && setInputStatus('')
    }

    return (
        <>
            <ModalWidget confirmLoading={confirmLoading} visible={visible} title={"Add New Marker"} onOk={handleOk} onCancel={handleCancel}>
                <FormWidget labelCol={{span: 5}} wrapperCol={{span: 17}}>
                            <FormWidget.Item label={'Label'}>
                                <InputWidget
                                    ref={initInput}
                                    value={markerLabelValue}
                                    onChange={handleMarkerLabelChange}
                                    status={inputStatus}
                                    onKeyPress={(event) => {
                                        if (event.key === "Enter") {
                                            handleOk()
                                        }
                                    }}
                                />
                            </FormWidget.Item>
                            <FormWidget.Item label={'Contact'}>
                                <StyledIconButton >
                                    {''}
                                    <StyledButtonSvgWrapper>
                                        <UserCircleIcon />
                                    </StyledButtonSvgWrapper>
                                </StyledIconButton>
                            </FormWidget.Item>
                </FormWidget>
            </ModalWidget>
        </>
    );
};

export default MarkerAddNewDialog;