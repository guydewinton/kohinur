import { ColWidget } from "./Col";
import { RowWidget } from "./Row";
import 'antd/es/divider/style'
import {DividerWidget} from "../Divider/Divider";


export default {
  title: "Base / Antd / Grid",
};
export const BasicGrid = () => {

  return (
      <>
          <RowWidget gutter={[20, 10]}>
              <ColWidget span={24}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-24</div></ColWidget>
              <ColWidget span={12}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-12</div></ColWidget>
              <ColWidget span={12}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-12</div></ColWidget>
              <ColWidget span={8}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-8</div></ColWidget>
              <ColWidget span={8}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-8</div></ColWidget>
              <ColWidget span={8}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-8</div></ColWidget>
              <ColWidget span={6}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-6</div></ColWidget>
              <ColWidget span={6}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-6</div></ColWidget>
              <ColWidget span={6}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-6</div></ColWidget>
              <ColWidget span={6}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>col-6</div></ColWidget>
          </RowWidget>
      </>
  )
};
BasicGrid.storyName = "Basic Grid";

export const FlexGrid = () => {

    return (
        <>
            <DividerWidget orientation="left" orientationMargin="0">Percentage columns</DividerWidget>
            <RowWidget>
                <ColWidget flex={2}><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>2 / 5</div></ColWidget>
                <ColWidget flex={3}><div style={{backgroundColor: 'skyblue', width: '100%', padding: '0 5px'}}>3 / 5</div></ColWidget>
            </RowWidget>
            <DividerWidget orientation="left" orientationMargin="0">Fill rest</DividerWidget>
            <RowWidget>
                <ColWidget flex="100px"><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>100px</div></ColWidget>
                <ColWidget flex="auto"><div style={{backgroundColor: 'skyblue', width: '100%', padding: '0 5px'}}>Fill Rest</div></ColWidget>
            </RowWidget>
            <DividerWidget orientation="left" orientationMargin="0">Raw flex style</DividerWidget>
            <RowWidget>
                <ColWidget flex="1 1 200px"><div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>1 1 200px</div></ColWidget>
                <ColWidget flex="0 1 300px"><div style={{backgroundColor: 'skyblue', width: '100%', padding: '0 5px'}}>0 1 300px</div></ColWidget>
            </RowWidget>
            <DividerWidget orientation="left" orientationMargin="0">No wrap</DividerWidget>
            <RowWidget wrap={false}>
                <ColWidget flex="none">
                    <div style={{backgroundColor: 'deepskyblue', width: '100%', padding: '0 5px'}}>none</div>
                </ColWidget>
                <ColWidget flex="auto"><div style={{backgroundColor: 'skyblue', width: '100%', padding: '0 5px'}}>auto with no-wrap</div></ColWidget>
            </RowWidget>
        </>
    )
};
FlexGrid.storyName = "Flex Grid";
