import {PrimaryUUIDField} from "../../library/model/app/decorators/prop/identifier.prop.decorator";
import {VarCharField} from "../../library/model/app/decorators/prop/character.prop.decorator";
import MetaModel from "../../library/model/app/decorators/class/MetaModel.class.decorator";

@MetaModel()
export class RefModel {

    @PrimaryUUIDField()
    id: string;

    @VarCharField()
    name: string;

}
