const PolyViewFactory = (viewName: string, polyTableName: string, _subTables: string[]) => {
    return `
    CREATE VIEW ${viewName} AS
    SELECT *,
    -- loop here
          CASE
            WHEN "${polyTableName}".type = "" THEN ''
            ELSE ''
        END AS type
    FROM ${polyTableName}
    -- loop here
    INNER JOIN address a USING (address_id)
    INNER JOIN city USING (city_id)
    INNER JOIN country USING (country_id);
    `
}

// todo: this needs to be finished and the gql union type created to handle it
// returns an object with {id, type, model/data/payload} (or something)

export default PolyViewFactory