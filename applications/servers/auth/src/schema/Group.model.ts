import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne} from "typeorm";
import {User} from "./User.model";

export enum UserGroups {
    PUBLIC = 'PUBLIC',
    MEMBER = 'MEMBER',
    STAFF = 'STAFF',
    ADMIN = 'ADMIN',
}

@Entity()
export class Group extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ManyToOne(() => User, user => user.groups)
    user: User;

    @Column({type: 'enum', enum: UserGroups, default: UserGroups.PUBLIC})
    group: UserGroups;

    @Column({type: 'varchar', nullable: true})
    date: string;

}
