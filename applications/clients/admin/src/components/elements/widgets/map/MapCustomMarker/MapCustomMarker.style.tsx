import styled from "styled-components";
import {Popup} from "react-leaflet";
import {ButtonWidget} from "../../../base/antd/general/Button/Button";

export const MapCustomMarkerWrapper = styled.div`
  
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  .leaflet-container {
    height: 100%;
    width: 100%;
    cursor: default;
  }
`

export const StyledAddMarkerButtonWrapper = styled.div`
  position: absolute;
  display: flex;
  
  bottom: 50px;
  z-index: 1000;
`

export const StyledAddMarkerButton = styled(ButtonWidget)`
  display: flex;
`



export const StyledPopup = styled(Popup)`
  position: relative;
  top: -100px;
  div {
    display: flex;
    flex-direction: column;
    align-items: center;
    p {
      padding: 0;
      margin: 0;
    }
  }
`