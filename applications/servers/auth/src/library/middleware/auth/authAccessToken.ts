import {NextFunction, Request, Response} from "express";

import userGroupPermission from "../../helpers/access/userGroupPermission";
import {validateAccessToken} from "../../helpers/crypto/token/jwt/defaults/jwtValidator";

const authAccessToken = async (_req: Request, res: Response, next: NextFunction) => {

    if (!validateAccessToken) {
        console.log('Invalid Access Token')
        return res.status(401).send('Invalid Access Token')
    }

    if (!userGroupPermission(res.locals.routeAccessGroups, res.locals.accessTokenBody.groups)) {
        console.log('Insufficient Permissions')
        return res.status(401).send('Insufficient Permissions')
    }

    res.locals.status = 201

    return next()

};

export default authAccessToken;
