import {ModelOptions} from "../../../../../type/options.type";
import {GraphqlModel} from "../../../api/gql/decorators/class/gql.class.decorator";
import {PolyEntity} from "../../../api/orm/decorators/class/poly.orm.class.decorator";
import {HasuraTable} from "../../../../action/external/hasura/class/table.class.hasura";

const PolyModel = (models: Function[], options?: ModelOptions | undefined) => {
    return <T extends {new(...args:any[]):{}}>(constructor:T) => {
        PolyEntity(models, options, constructor)
        GraphqlModel(constructor, options)
        HasuraTable(constructor)
        return constructor
    }
}

export default PolyModel