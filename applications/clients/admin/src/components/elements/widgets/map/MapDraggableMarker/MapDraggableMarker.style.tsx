import styled from "styled-components";

export const MapDraggableMarkerWrapper = styled.div`
  
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  .leaflet-container {
    height: 100%;
    width: 100%;
    cursor: default;
  }
`
