import styled from "styled-components";

export const MarkerPaneWrapper = styled.div`
  height: 100%;
  width: 350px;
  display: flex;
  flex-direction: column;
  background-color: #f5f5f5;

`

export const MarkerFilterWrapper = styled.div`
  //height: 250px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  align-items: flex-start;
`

export const MarkerJobFilterWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  align-items: flex-start;
  padding: 0.5em 1em;
  border: 1px #ababab solid;
`

export const PaddingWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 0 1em;
`

export const MarkerTableHeightOuter = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  //max-height: 100%;
`

export const MarkerTableHeightInner = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  //max-height: 100%;
`

export const VerticalSpace = styled.div`
  height: 1em;
`