import styled from "styled-components";
import {Row} from "antd/es";
import "antd/es/row/style"

const StyledRow = styled(Row)`
`

export const RowWidget = StyledRow

export type { RowProps } from 'antd/es/row';