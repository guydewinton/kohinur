import {PrimaryBaseModel} from "../../abstract/primary.base.model";
import {VarCharField} from "../../../library/model/app/decorators/prop/character.prop.decorator";
import PrimaryModel from "../../../library/model/app/decorators/class/PrimaryModel.class.decorator";

@PrimaryModel()
export class Member extends PrimaryBaseModel {

    @VarCharField()
    displayName: string;

}
