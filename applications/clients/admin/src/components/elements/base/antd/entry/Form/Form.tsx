import styled from "styled-components";

import {Form} from "antd/es";
import "antd/es/form/style"

const StyledForm = styled(Form)`
`

export const FormWidget = StyledForm

export type { FormInstance, FormProps, FormItemProps, ErrorListProps, Rule, RuleObject, RuleRender, FormListProps, } from 'antd/es/form'