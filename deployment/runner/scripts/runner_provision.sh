# add base-dependencies
sudo apt-get update
sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# add docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

# add docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# add gitlab runner
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

sudo chmod +x /usr/local/bin/gitlab-runner

sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

sudo apt-get update

sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

# start runner
sudo gitlab-runner start

# register dev runner
sudo gitlab-runner register \
  --url https://gitlab.com/ \
  --registration-token azgWrJ8xYNC5dx2gYvuK \
  --executor docker \
  --docker-image docker/compose \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-volumes "/home/gitlab-runner/docker-cache" \
  --tag-list "dev" \
  -n


# register test runner
sudo gitlab-runner register \
  --url https://gitlab.com/ \
  --registration-token azgWrJ8xYNC5dx2gYvuK \
  --executor docker \
  --docker-image docker/compose \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-volumes "/home/gitlab-runner/docker-cache" \
  --tag-list "test" \
  -n

# register deploy runner
sudo gitlab-runner register \
  --url https://gitlab.com/ \
  --registration-token azgWrJ8xYNC5dx2gYvuK \
  --executor docker \
  --docker-image docker/compose \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-volumes "/home/gitlab-runner/docker-cache" \
  --tag-list "deploy" \
  -n
