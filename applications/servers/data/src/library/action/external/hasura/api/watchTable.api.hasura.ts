import axios from "axios";

export const watchTable = async(tableName: string) => {
    await axios('http://hasura-data:8080/v1/metadata', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Hasura-Role': 'admin'
        },
        data: JSON.stringify({
            "type": "pg_track_table",
            "args": {
                "source": 'data',
                "schema": "public",
                "table": tableName
            }
        })
    }).catch(function (error) {
        console.log('Error', error.message);
        console.log(error.response.data)
    })
}