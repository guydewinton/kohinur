export interface CheckboxOptions {
    value: string;
    label: string;
    disabled?: boolean;
    checked: boolean;
}