import styled from "styled-components";
import PageHeader from "antd/es/page-header";
import 'antd/es/page-header/style'

const StyledPageHeader = styled(PageHeader)``

export const PageHeaderWidget = StyledPageHeader

export type {PageHeaderProps} from'antd/es/page-header