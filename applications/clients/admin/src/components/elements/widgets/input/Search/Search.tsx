import React from "react";
import styled from "styled-components";
import {InputWidget} from "../../../base/antd/entry/Input/Input";

const StyledSearch = styled(InputWidget.Search)`
    padding: 0 1em;
`

interface SearchWidgetProps {
    onChange: any
    placeholder?: string
    value: string
}

export const SearchWidget: React.FC<SearchWidgetProps> = ({
            onChange,
            placeholder,
            value
        }) => {
    return (
        <StyledSearch
            placeholder={placeholder ? placeholder : ''}
            allowClear
            value={value}
            onChange={onChange}
        />
    )
}