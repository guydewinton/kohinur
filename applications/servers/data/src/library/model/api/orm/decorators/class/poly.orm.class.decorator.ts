import {Entity} from "typeorm";

import {sqlTriggerList} from "../../../sql/plpg/triggers/triggerList";
import {polyInsertDeleteHandler} from "../../../sql/plpg/functions/polyHandlers";
import {polyDeleteTrigger, polyInsertTrigger} from "../../../sql/plpg/triggers/polyTriggers";
import {ModelOptions} from "../../../../../../type/options.type";


export const PolyEntity = (models: Function[], options: ModelOptions | undefined, constructor: Function) => {

    sqlTriggerList.push(polyInsertDeleteHandler(constructor));

    for (let i: number = 0; i < models.length; i++) {
        sqlTriggerList.push(polyInsertTrigger(models[i]));
        sqlTriggerList.push(polyDeleteTrigger(models[i]));
    }

    Entity(options)(constructor);


};