import styled from "styled-components";

import {Checkbox} from "antd/es";
import "antd/es/checkbox/style"

const StyledCheckbox = styled(Checkbox)`
`

export const CheckboxWidget = StyledCheckbox

export type { CheckboxProps, CheckboxChangeEvent } from 'antd/es/checkbox';
