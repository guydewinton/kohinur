import {EntityOptions} from "typeorm/decorator/options/EntityOptions";

export declare function Entity(options?: EntityOptions): ClassDecorator;

export declare function Entity(name?: string, options?: EntityOptions): ClassDecorator;