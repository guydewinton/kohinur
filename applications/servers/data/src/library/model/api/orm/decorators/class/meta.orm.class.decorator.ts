import {ModelOptions} from "../../../../../../type/options.type";
import {Entity} from "typeorm";

export const MetaEntity = (name: string | undefined, options: ModelOptions, constructor: Function) => {
    Entity(name, options)(constructor)
}
