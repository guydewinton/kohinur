import {TypeGqlBaseOptions} from "../../../../../../type/options.type";
import {Field} from "type-graphql";
import { GraphQLScalarType } from "graphql";

export const GraphqlField = (prototype: Object, propertyName: string, type: GraphQLScalarType | Function, options?: TypeGqlBaseOptions) => {
    const graphqlOptions = {} as TypeGqlBaseOptions
    options?.name ? graphqlOptions.name = options.name : null;
    options?.comment ? graphqlOptions.description = options.comment : null;
    options?.nullable ? graphqlOptions.nullable = options.nullable : null;

    if (options?.select !== false) {
        if (options?.array) {
            Field(() => [type], graphqlOptions)(prototype, propertyName)
        } else {
            Field(() => type, graphqlOptions)(prototype, propertyName)
        }
    }
}

export const GraphqlSingleRelation = (prototype: Object, propertyName: string, type: Function, options?: TypeGqlBaseOptions) => {
    const graphqlOptions = {} as TypeGqlBaseOptions
    options?.name ? graphqlOptions.name = options.name : null;
    options?.comment ? graphqlOptions.description = options.comment : null;
    options?.nullable ? graphqlOptions.nullable = options.nullable : null;
    Field(() => type, graphqlOptions)(prototype, propertyName)
}

export const GraphqlManyRelation = (prototype: Object, propertyName: string, type: Function, options?: TypeGqlBaseOptions) => {
    const graphqlOptions = {} as TypeGqlBaseOptions
    options?.name ? graphqlOptions.name = options.name : null;
    options?.comment ? graphqlOptions.description = options.comment : null;
    options?.nullable ? graphqlOptions.nullable = options.nullable : null;
    Field(() => [type], graphqlOptions)(prototype, propertyName)
}