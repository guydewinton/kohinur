import {hasuraRelationsCache, hasuraRelationsList, hasuraTableList} from "../queue/queues.hasura";
import {getModelTableName} from "../../../internal/TypeOrm/model/helpers";
import {watchTable} from "../api/watchTable.api.hasura";

const processHasuraTableWatchList = async () => {
    for (let i: number = 0; i < hasuraTableList.length; i++) {
        const tableName = await getModelTableName(hasuraTableList[i])
        await watchTable(tableName)
    }
}

export const HasuraTable = (constructor: Function) => {
    hasuraTableList.push(constructor)
    hasuraRelationsCache.current.forEach((relationProp) => {
        hasuraRelationsList.push({constructor, ...relationProp})
    })
    hasuraRelationsCache.current = []
}

export default processHasuraTableWatchList;

