import {Contact} from "../contact.model";
import {SecondaryBaseModel} from "../../../abstract/secondary.base.model";
import {VarCharField} from "../../../../library/model/app/decorators/prop/character.prop.decorator";
import {ManyToOneRelation} from "../../../../library/model/app/decorators/prop/relation.prop.decorator";
import SecondaryModel from "../../../../library/model/app/decorators/class/SecondaryModel.class.decorator";

@SecondaryModel()
export class Address extends SecondaryBaseModel {

    @VarCharField()
    name: string

    @VarCharField({array: true})
    streetAddress: string[]

    @VarCharField()
    suburb: string

    @VarCharField()
    postcode: string

    @VarCharField()
    country: string

    @ManyToOneRelation(() => Contact, contact => contact.addresses)
    contact: Contact

}