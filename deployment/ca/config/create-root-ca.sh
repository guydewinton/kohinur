openssl genrsa -out /certs/privkey.pem 4096

openssl req -config /etc/openssl.conf \
    -new -x509 -sha256 \
    -days 365 \
    -nodes \
    -key /certs/privkey.pem \
    -out /certs/fullchain.pem \
    -subj "/C=$CA_COUNTRY/ST=$CA_STATE/L=$CA_LOCALITY/O=$CA_ORGANISATION/CN=$CA_SERVER"

chmod 777 /certs/fullchain.pem
chmod 777 /certs/privkey.pem