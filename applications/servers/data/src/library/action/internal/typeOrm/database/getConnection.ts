import {Connection, getConnection} from "typeorm";

export const DBConnection = async () => {
    let entityManager: Connection;
    try {
        entityManager = await getConnection();
    } catch (err) {
        console.log(err)
        throw err;
    }
    return entityManager
}