import styled from "styled-components";
import React from "react";

const BodyWrapper = styled.div`
  height: calc(100% - 70px);
  width: 100%;
  background-color: #b2b2b2;
  display: flex;
  position: relative;
`

const BodyMatte = styled.div<{bodyMask: boolean}>`
  position: absolute;
  height: 100%;
  width: 100%;
  background-color: ${props => props.bodyMask ? `rgba(49, 49, 49, 0.8)` : `rgba(49, 49, 49, 0)`};
  z-index: 10000;
  transition: background-color 300ms;
`

export const Body:React.FC<{bodyMask: boolean}> = ({children, bodyMask}) => {
    return (
        <BodyWrapper>
            <BodyMatte bodyMask={bodyMask}/>
            {children}
        </BodyWrapper>
    )
}