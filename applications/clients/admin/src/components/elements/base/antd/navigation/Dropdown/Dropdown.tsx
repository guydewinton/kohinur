import styled from "styled-components";
import Dropdown from "antd/es/dropdown";
import "antd/es/dropdown/style";

const StyledDropdown = styled(Dropdown)`
  .ant-dropdown-arrow::before {
    background-color: #0070f3;
  }
`

export const DropdownWidget = StyledDropdown

export type {DropDownProps, DropdownButtonProps, DropdownButtonType} from 'antd/es/dropdown'