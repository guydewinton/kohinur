import React, {useEffect, useState} from 'react';
import {Admin} from "./application/Admin";
import {BrowserRouter, MemoryRouter} from "react-router-dom";
import styled from "styled-components";
import useWindowSize from "./library/hooks/useWindowSize";

const AppWrapper = styled.div<{ width: number, height: number, pxSize: number }>`
  height: ${props => props.height ? props.height : 0}px;
  width: ${props => props.width ? props.width : 0}px;
  //font-size: ${props => props.pxSize ? props.pxSize : 1}px;
`

function App() {

    const {height, width} = useWindowSize()
    const [pxSize, setPxSize] = useState(1)

    useEffect(() => {
        setPxSize(height / 900)
    }, [height])

    return (
        <AppWrapper width={width} height={height} pxSize={pxSize}>
            <MemoryRouter>
                <Admin/>
            </MemoryRouter>
        </AppWrapper>
    );
}

export default App;
