import React, {useEffect} from "react";

function useWindowSize() {

    const [dimensions, setDimensions] = React.useState({
        height: window.innerHeight,
        width: window.innerWidth
    })

    useEffect(() => {

        const debouncedHandleResize = (function () {
            setDimensions({
                height: window.innerHeight,
                width: window.innerWidth
            })
        })

        window.addEventListener('resize', debouncedHandleResize as any)

        return () => {
            window.removeEventListener('resize', debouncedHandleResize as any)
        }

    }, [])

    return dimensions;
}

export default useWindowSize