import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany} from "typeorm";
import {IsEmail} from 'class-validator'
import {Group} from "./Group.model";

export enum UserStatus {
    ACTIVE = 'ACTIVE',
    INACTIVE = 'INACTIVE',
    PENDING = 'PENDING',
}

@Entity()
export class User extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @IsEmail()
    @Column({type: 'varchar', unique: true})
    email: string;

    @Column({type: 'varchar'})
    password: string;

    @Column({type: 'varchar', nullable: true})
    refreshToken: string;

    @OneToMany(() => Group, group => group.user, {cascade: true, eager: true})
    groups: Group[];

    @Column({type: 'enum', enum: UserStatus, default: UserStatus.PENDING})
    status: UserStatus;

}
