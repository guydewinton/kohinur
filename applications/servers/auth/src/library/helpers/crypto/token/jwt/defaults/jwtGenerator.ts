import {sign} from "jsonwebtoken";

export const generateAccessToken = (userId: string, status: string, groups: string[], secret: string, expires: string) => {
        return sign({userId, status, groups}, secret, {expiresIn: expires})
    }

export const generateRefreshToken = (userId: string, secret: string, expires: string) => {
        return sign({userId}, secret, {expiresIn: expires})
    }

export const generateServiceToken = (redirect: string, secret: string, expires: string) => {
        return sign({redirect}, secret, {expiresIn: expires})
    }