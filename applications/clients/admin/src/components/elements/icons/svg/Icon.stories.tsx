import BoxIcon from "./Box/Box";
import BoxesIcon from "./Boxes/Boxes";
import AngleArrowElementIndex from "./AngleArrow/AngleArrow.element.index";
import AngleArrowIcon from "./AngleArrow/AngleArrow.element.index";
import {BuildingIcon} from "./Building/Building";
import CalendarCheckedIcon from "./CalendarChecked/CalendarChecked";
import CashRegisterIcon from "./CashRegister/CashRegister";
import {CircleIcon} from "./Circle/Circle";
import CommentIcon from "./Comment/Comment";
import EllipsisHIcon from "./EllipsisH/EllipsisH";
import {FlagIcon} from "./Flag/Flag";
import {GangaIcon} from "./Ganga/Ganga";
import HandshakeIcon from "./Handshake/Handshake";
import {HousePinIcon} from "./HousePin/HousePin";
import InfoIcon from "./Info/Info";
import {MapPinIcon} from "./MapPin/MapPin";
import {MapPinCustomIcon} from "./MapPinCustom/MapPinCustom";
import {PersonIcon} from "./Person/Person";
import {RoadIcon} from "./Road/Road";
import ShoppingBasketIcon from "./ShoppingBasket/ShoppingBasket";
import StarIcon from "./Star/Star";
import {ToolboxIcon} from "./Toolbox/Toolbox";
import {ToolsIcon} from "./Tools/Tools";
import TruckIcon from "./Truck/Truck";
import {UserCircleIcon} from "./UserCircle/UserCircle";
import UsersIcon from "./Users/Users";
import styled from "styled-components";
import XMarkIcon from "./XMark/XMark";
import AddressBookIcon from "./AddressBook/AddressBook";
import ListItemsIcon from "./ListItems/ListItems";
import CheckboxIcon from "./Checkbox/Checkbox";
import CalendarSolidIcon from "./CalendarSolid/CalendarSolid";

const IconWrapper = styled.div`
  height: 100px;
  width: 100px;
`

export default {
  title: "Icons / All",
};

export const AddressBook = () => <IconWrapper><AddressBookIcon/></IconWrapper>
export const AngleArrow = () => <IconWrapper><AngleArrowIcon/></IconWrapper>
export const Box = () => <IconWrapper><BoxIcon/></IconWrapper>
export const Boxes = () => <IconWrapper><BoxesIcon/></IconWrapper>
export const Building = () => <IconWrapper><BuildingIcon/></IconWrapper>
export const CalendarChecked = () => <IconWrapper><CalendarCheckedIcon/></IconWrapper>
export const CalendarSolid = () => <IconWrapper><CalendarSolidIcon/></IconWrapper>
export const CashRegister = () => <IconWrapper><CashRegisterIcon/></IconWrapper>
export const Checkbox = () => <IconWrapper><CheckboxIcon/></IconWrapper>
export const Circle = () => <IconWrapper><CircleIcon/></IconWrapper>
export const Comment = () => <IconWrapper><CommentIcon/></IconWrapper>
export const EllipsisH = () => <IconWrapper><EllipsisHIcon/></IconWrapper>
export const Flag = () => <IconWrapper><FlagIcon/></IconWrapper>
export const Ganga = () => <IconWrapper><GangaIcon/></IconWrapper>
export const Handshake = () => <IconWrapper><HandshakeIcon/></IconWrapper>
export const HousePin = () => <IconWrapper><HousePinIcon/></IconWrapper>
export const Info = () => <IconWrapper><InfoIcon/></IconWrapper>
export const ListItems = () => <IconWrapper><ListItemsIcon/></IconWrapper>
export const MapPin = () => <IconWrapper><MapPinIcon/></IconWrapper>
export const Person = () => <IconWrapper><PersonIcon/></IconWrapper>
export const Road = () => <IconWrapper><RoadIcon/></IconWrapper>
export const ShoppingBasket = () => <IconWrapper><ShoppingBasketIcon/></IconWrapper>
export const Star = () => <IconWrapper><StarIcon/></IconWrapper>
export const Toolbox = () => <IconWrapper><ToolboxIcon/></IconWrapper>
export const Tools = () => <IconWrapper><ToolsIcon/></IconWrapper>
export const Truck = () => <IconWrapper><TruckIcon/></IconWrapper>
export const UserCircle = () => <IconWrapper><UserCircleIcon/></IconWrapper>
export const Users = () => <IconWrapper><UsersIcon/></IconWrapper>
export const XMark = () => <IconWrapper><XMarkIcon/></IconWrapper>

