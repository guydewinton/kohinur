import {PrimaryBaseModel} from "../../abstract/primary.base.model";
import {ContactSet} from "../contact/contact.set.model";
import {VarCharField} from "../../../library/model/app/decorators/prop/character.prop.decorator";
import {OneToManyRelation} from "../../../library/model/app/decorators/prop/relation.prop.decorator";
import PrimaryModel from "../../../library/model/app/decorators/class/PrimaryModel.class.decorator";

@PrimaryModel()
export class Company extends PrimaryBaseModel {

    @VarCharField()
    displayName: string;

    @VarCharField()
    name: string;

    @OneToManyRelation(() => ContactSet, (contactSet) => contactSet.contactable)
    contacts: ContactSet[]
}
