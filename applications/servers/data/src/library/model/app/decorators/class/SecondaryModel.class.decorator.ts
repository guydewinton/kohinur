import {ModelOptions} from "../../../../../type/options.type";
import {SecondaryEntity} from "../../../api/orm/decorators/class/base.orm.class.decorator";
import {GraphqlModel} from "../../../api/gql/decorators/class/gql.class.decorator";
import {HasuraTable} from "../../../../action/external/hasura/class/table.class.hasura";

const SecondaryModel = (nameOrOptions?: string | undefined, maybeOptions?: ModelOptions | undefined) => {
    const options = (typeof nameOrOptions === "object" ? nameOrOptions : maybeOptions) || {};
    let name = typeof nameOrOptions === "string" ? nameOrOptions : options.name;
    return <T extends {new(...args:any[]):{}}>(constructor:T) => {
        SecondaryEntity(name, options, constructor)
        GraphqlModel(constructor, options)
        HasuraTable(constructor)
        return constructor
    }
}

export default SecondaryModel