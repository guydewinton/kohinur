import {Router} from "express";
import userRouter from "./user.http.route";
import authRouter from "./auth.http.route";

const rootRouter = Router();

rootRouter.use('/user', userRouter);
rootRouter.use('/auth', authRouter);

export default rootRouter;