import React from "react";
import {Link} from "react-router-dom";
import styled from "styled-components";

const SidebarWrapper = styled.div`
  width: 280px;
  height: 100%;
  background-color: #eaeaea;
  display: flex;
  flex-direction: column;
  padding: 18px 0;
  border-right: solid 1px #d0d0d0;

  .selected {
    background-color: #364a57; //364a57

    :hover {
      background-color: #425b6e;
    }

    h3 {
      color: #eaf0fd;
      font-weight: bold;
    }
  }


`

const SideBarItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  transition: background-color 100ms;

  svg {
    fill: #525252;
  }

  padding: 10px 35px;
  margin: 3px 0;

  :hover {
    background-color: #7798b2;

    h3 {
      color: #eaeaea;
      font-weight: bold;
    }
  }


`

const SideBarTextWrapper = styled.div``

const SideBarText = styled.h3`
  color: #525252; 
  font-size: 20px;
  margin: 0;
`

export const Sidebar: React.FC = () => {
    return (
        <SidebarWrapper>
            <Link to={'recovery'}>
                <SideBarItemWrapper className={'selected'}>
                    <SideBarTextWrapper>
                        <SideBarText>Recovery</SideBarText>
                    </SideBarTextWrapper>
                </SideBarItemWrapper>
            </Link>
            <Link to={'inventory'}>
                <SideBarItemWrapper>

                    <SideBarTextWrapper>
                        <SideBarText>Inventory</SideBarText>
                    </SideBarTextWrapper>
                </SideBarItemWrapper>
            </Link>
            <Link to={'logistics'}>
                <SideBarItemWrapper>
                    <SideBarTextWrapper>
                        <SideBarText>Logistics</SideBarText>
                    </SideBarTextWrapper>
                </SideBarItemWrapper>
            </Link>
            <Link to={'roster'}>
                <SideBarItemWrapper>
                    <SideBarTextWrapper>
                        <SideBarText>Roster</SideBarText>
                    </SideBarTextWrapper>
                </SideBarItemWrapper>
            </Link>
            <Link to={'map'}>
                <SideBarItemWrapper>
                    <SideBarTextWrapper>
                        <SideBarText>Maps</SideBarText>
                    </SideBarTextWrapper>
                </SideBarItemWrapper>
            </Link>
        </SidebarWrapper>
    )
}