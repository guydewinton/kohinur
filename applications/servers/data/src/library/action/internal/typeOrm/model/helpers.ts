import {DBConnection} from "../database/getConnection";
import {HasuraRelationMetadataObject} from "../../../external/hasura/type/types.hasura";

export const getModelTableName = async (model: Function) => {
    const entityManager = await DBConnection();
    const repo = await entityManager.getRepository(model)
    const tableName = repo.metadata.tableName
    return tableName
}


export const getRelationalMetadata = async (model: Function, propName: string, inverse: boolean = false) => {
    const entityManager = await DBConnection();
    const returnMetadata: HasuraRelationMetadataObject = {};
    const repo = await entityManager.getRepository(model)
    const relations = repo.metadata.relations
    for (let i = 0; i < relations.length; i++) {
        const relation = relations[i]
        if (relation.propertyName === propName) {
            returnMetadata.tableName = relation.entityMetadata.tableName;
            returnMetadata.columnName = relation.propertyName;
            if (inverse && relation.inverseRelation) {
                returnMetadata.inverseTableName = relation.inverseRelation.entityMetadata.tableName;
                returnMetadata.inverseColumnName = relation.inverseRelation.propertyName;
                returnMetadata.inverseColumnNameDB = relation.inverseRelation.joinColumns[0].databaseName;
            } else {
                returnMetadata.columnNameDB = relation.joinColumns[0].databaseName;
            }
        }
    }
    return returnMetadata;
}