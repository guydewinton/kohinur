import {HasuraRelationCacheObject, HasuraRelationObject, HasuraModelList} from "../type/types.hasura";

export const hasuraTableList: HasuraModelList = []

export const hasuraRelationsList: HasuraRelationObject[] = []

export const hasuraRelationsCache: HasuraRelationCacheObject = {current: []}
