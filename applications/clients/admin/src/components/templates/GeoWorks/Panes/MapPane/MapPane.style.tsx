import styled from "styled-components";

export const MapWrapper = styled.div`
  
  height: 100%;
  width: 100%;
  .leaflet-container {
    height: 100%;
    width: 100%;
    cursor: default;
  }
`



export const StyledMapPopupLink = styled.p`
  //margin-top: 15px;
  cursor: pointer;
  color: blue;
`