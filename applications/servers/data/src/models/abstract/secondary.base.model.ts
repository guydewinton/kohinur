import {AbstractBaseModel} from "./abstract.base.model";
import {PrimaryGeneratedUUIDField} from "../../library/model/app/decorators/prop/identifier.prop.decorator";

export abstract class SecondaryBaseModel extends AbstractBaseModel {

    @PrimaryGeneratedUUIDField()
    id: string;

}