import styled from "styled-components";
import Breadcrumb from "antd/es/breadcrumb";
import 'antd/es/breadcrumb/style'

const StyledBreadcrumb = styled(Breadcrumb)``

export const BreadcrumbWidget = StyledBreadcrumb

export type {BreadcrumbProps} from 'antd/es/breadcrumb'