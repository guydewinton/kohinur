import {ButtonWidget} from "../../general/Button/Button";
import { BreadcrumbWidget } from "../../navigation/Breadcrumb/Breadcrumb";
import {MenuProps, MenuWidget } from "../../navigation/Menu/Menu";
import { LayoutWidget } from "./Layout";
import React from "react";
import { HeaderWidget } from "./Header";
import { SiderWidget } from "./Sider";
import { ContentWidget } from "./Content";
import { UserOutlined, LaptopOutlined, NotificationOutlined, MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import styled from "styled-components";

const StyledLayoutWidget = styled(LayoutWidget)`
  #components-layout-demo-top-side-2 .logo {
    float: left;
    width: 120px;
    height: 31px;
    margin: 16px 24px 16px 0;
    background: rgba(255, 255, 255, 0.3);
  }

  .ant-row-rtl #components-layout-demo-top-side-2 .logo {
    float: right;
    margin: 16px 0 16px 24px;
  }

  .site-layout-background {
    background: #fff;
  }

  .ant-layout-sider-zero-width-trigger {
    top: 0;
  }
`


export default {
    title: "Base / Antd / Layout",
};
export const Layout = () => {


    return (
        <StyledLayoutWidget>
            <HeaderWidget className="header">
                <div className="logo" />
            </HeaderWidget>
            <LayoutWidget>
                <SiderWidget width={200} className="site-layout-background" collapsedWidth={0} breakpoint="lg">
                    <MenuWidget>
                        <MenuWidget.Item icon={<UserOutlined/>}>item 1</MenuWidget.Item>
                        <MenuWidget.Item icon={<LaptopOutlined/>}>item 2</MenuWidget.Item>
                        <MenuWidget.SubMenu title="sub menu" icon={<MailOutlined/>}>
                            <MenuWidget.Item icon={<NotificationOutlined/>}>item 3</MenuWidget.Item>
                        </MenuWidget.SubMenu>
                    </MenuWidget>
                </SiderWidget>
                <LayoutWidget style={{ padding: '0 24px 24px' }}>
                    <BreadcrumbWidget style={{ margin: '16px 0' }}>
                        <BreadcrumbWidget.Item>Home</BreadcrumbWidget.Item>
                        <BreadcrumbWidget.Item>List</BreadcrumbWidget.Item>
                        <BreadcrumbWidget.Item>App</BreadcrumbWidget.Item>
                    </BreadcrumbWidget>
                    <ContentWidget
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                        }}
                    >
                        Content
                    </ContentWidget>
                </LayoutWidget>
            </LayoutWidget>
        </StyledLayoutWidget>
    )
}
Layout.storyName = "Basic Layout";

export const LayoutTwo = () => {


    return (
        <StyledLayoutWidget>
            <LayoutWidget>
                <SiderWidget width={200} className="site-layout-background" collapsedWidth={0} breakpoint="lg">
                    <MenuWidget>
                        <MenuWidget.Item icon={<UserOutlined/>}>item 1</MenuWidget.Item>
                        <MenuWidget.Item icon={<LaptopOutlined/>}>item 2</MenuWidget.Item>
                        <MenuWidget.SubMenu title="sub menu" icon={<MailOutlined/>}>
                            <MenuWidget.Item icon={<NotificationOutlined/>}>item 3</MenuWidget.Item>
                        </MenuWidget.SubMenu>
                    </MenuWidget>
                </SiderWidget>
                <LayoutWidget style={{ padding: '35px 35px' }}>
                    <BreadcrumbWidget style={{ margin: '16px 0' }}>
                        <BreadcrumbWidget.Item>Home</BreadcrumbWidget.Item>
                        <BreadcrumbWidget.Item>List</BreadcrumbWidget.Item>
                        <BreadcrumbWidget.Item>App</BreadcrumbWidget.Item>
                    </BreadcrumbWidget>
                    <ContentWidget
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                        }}
                    >
                        Content
                    </ContentWidget>
                </LayoutWidget>
            </LayoutWidget>
        </StyledLayoutWidget>
    )
}
LayoutTwo.storyName = "Collapsable Layout";