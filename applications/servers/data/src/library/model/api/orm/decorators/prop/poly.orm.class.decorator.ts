import {sqlTriggerList} from "../../../sql/plpg/triggers/triggerList";
import {polyDeleteTrigger, polyInsertTrigger} from "../../../sql/plpg/triggers/polyTriggers";


export const PolyProp = (model: Function) => {

    sqlTriggerList.push(polyInsertTrigger(model));
    sqlTriggerList.push(polyDeleteTrigger(model));

};