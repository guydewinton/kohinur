export type HasuraModelList = Function[]

export interface HasuraRelationProp {
    propName: string;
    inverse: boolean;
}

export interface HasuraRelationObject extends HasuraRelationProp{
    constructor: Function;
}

export interface HasuraRelationMetadataObject {
    tableName?: string
    columnName?: string
    columnNameDB?: string
    inverseTableName?: string
    inverseColumnName?: string
    inverseColumnNameDB?: string
}

export interface HasuraRelationCacheObject {
    current: HasuraRelationProp[]
}