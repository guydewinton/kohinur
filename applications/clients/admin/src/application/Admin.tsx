import React, {useState} from "react";
import {Sidebar} from "../components/application/Sidebar/Sidebar";
import {MainPane} from "../components/application/MainPane/MainPane";
import styled from "styled-components";
import {Header} from "../components/application/Header/Header";
import {Body} from "../components/application/Body/Body";
import {GeoWorksModule} from "./modules/GeoWorks/GeoWorks.module";

const AdminWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  //background-color: #d3d3d3;
`

export const Admin: React.FC = () => {

    const [bodyMask, setBodyMask] = useState<boolean>(false)


    return (
        <AdminWrapper>
            <Header setBodyMask={setBodyMask}/>
            <Body bodyMask={bodyMask}>
                <Sidebar/>
                <MainPane>
                    <GeoWorksModule/>
                </MainPane>
            </Body>

        </AdminWrapper>
    )
}

// todo: sidebar and mainpane need rethinking...