import crypto from "crypto";

export const generateSecret = () => {
        return crypto.randomBytes(60).toString('hex');
    }