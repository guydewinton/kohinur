type ContactPointTypeUnion = 'telephone' | 'email'

interface ContactPointInfo {
    type: ContactPointTypeUnion
    contact?: string;
}

interface AddressDict {

}


interface ContactBaseDict {
    type: 'resident' | 'trade'
    alias: string
    givenName?: string;
    surname?: string;
    addresses?: AddressDict[]
    contacts?: ContactPointInfo[]
}

export interface ContactTradeDict extends ContactBaseDict {
    type: 'trade'
    tradeSkill: string; // enum
}

export interface ContactResidentDict extends ContactBaseDict {
    type: 'resident'
}

export type ContactDictUnion = ContactTradeDict | ContactResidentDict

export type ContactDictUnionArray = ContactDictUnion[]

