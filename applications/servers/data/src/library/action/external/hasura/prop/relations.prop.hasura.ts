import {createArrayRelation, createObjectRelation} from "../api/createRelation.api.hasura";
import {HasuraRelationMetadataObject} from "../type/types.hasura";
import {hasuraRelationsCache, hasuraRelationsList} from "../queue/queues.hasura";
import {getRelationalMetadata} from "../../../internal/TypeOrm/model/helpers";

export const processHasuraRelationsList = async () => {
    for (let i = 0; i < hasuraRelationsList.length; i++) {
        const relationMetaData: HasuraRelationMetadataObject = await getRelationalMetadata(
            hasuraRelationsList[i].constructor,
            hasuraRelationsList[i].propName,
            hasuraRelationsList[i].inverse,
        )
        if (hasuraRelationsList[i].inverse) {
            await createArrayRelation(relationMetaData.tableName!, relationMetaData.columnName!, relationMetaData.inverseTableName!, relationMetaData.inverseColumnNameDB!)

        } else {
            await createObjectRelation(relationMetaData.tableName!, relationMetaData.columnName!, relationMetaData.columnNameDB!)

        }
    }
}

export const HasuraRelation = (propName: string, inverse: boolean) => {
    hasuraRelationsCache.current.push({propName, inverse})
}
