import {Router} from "express";

import authAccessToken from "../../library/middleware/auth/authAccessToken";
import authRefreshToken from "../../library/middleware/auth/authRefreshToken";
import jwt from "../../library/helpers/crypto/token/jwt/jwt";
import parseRouteAccessGroupsHeader from "../../library/middleware/parse/parseRouteAccessGroupsHeader";
import {parseAccessTokenHeader} from "../../library/middleware/parse/parseAccessTokenHeader";
import {parseRefreshTokenCookie} from "../../library/middleware/parse/parseRefreshTokenCookie";
import {Group} from "../../schema/Group.model";
import {cookieOptions} from "../../library/token/cookie";


const accessRouter = Router();

accessRouter.get(
    '/validate',
    parseRouteAccessGroupsHeader,
    parseAccessTokenHeader,
    authAccessToken,
    async (_req, res) => {
        console.log('ACCESS VALIDATE')

        if (res.locals.status === 201) {
            return res.status(201).send()
        }
        return res.status(401).send('Access Token Validation Failed')
});

accessRouter.get(
    '/issue',
    parseRouteAccessGroupsHeader,
    parseRefreshTokenCookie,
    authRefreshToken,
    async (_req, res) => {

        if (res.locals.status === 201) {

            const refreshToken = jwt.generateRefreshToken(res.locals.user.id);
            res.locals.userGroupList = res.locals.user.groups.map((group: Group) => group.group)
            res.cookie('mfc', refreshToken, cookieOptions);

            const accessToken = jwt.generateAccessToken(res.locals.user.id, res.locals.user.status, res.locals.userGroupList,);
            return res.status(201).send(JSON.stringify({
                access: accessToken
            }));

        }

        return res.status(401).send('Access Token Issue Failed')

});


export default accessRouter;