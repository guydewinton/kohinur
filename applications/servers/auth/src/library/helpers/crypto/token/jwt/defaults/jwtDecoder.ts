import {JwtPayload} from "jsonwebtoken";

export const accessBodyGuard = (accessBody: JwtPayload) => (accessBody.userId || accessBody.status || accessBody.groups);

export const refreshBodyGuard = (accessBody: JwtPayload) => (accessBody.userId || accessBody.status || accessBody.groups);