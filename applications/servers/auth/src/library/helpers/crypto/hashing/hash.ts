import {randomBytes, scrypt, timingSafeEqual} from "crypto";
import {promisify} from "util";

const promiseScrypt = promisify(scrypt);

export async function createHash(password: string): Promise<string> {
    try {
        const salt = randomBytes(16).toString('hex')
        const derivedKey = await promiseScrypt(password, salt, 64) as Buffer;
        return salt + ":" + derivedKey.toString('hex')
    } catch (err) {
        console.log(err)
        throw err
    }
}


export async function verifyHash(password: string, hash: string) {
    try {
        const [salt, key] = hash.split(":")
        const keyBuffer = Buffer.from(key, 'hex')
        const derivedKey = await promiseScrypt(password, salt, 64) as Buffer;
        return timingSafeEqual(keyBuffer, derivedKey)
    } catch (err) {
        console.log(err)
        return false
    }
}






// import {hash, compare} from "bcryptjs";
//
// export const verifyHash = async (string: string, hash: string) => {
//     try {
//         return await compare(string, hash)
//     }
//     catch (err) {
//         console.log(err)
//         return false
//     }
// }
//
// export const createHash = async (string: string) => {
//     try {
//         return await hash(string, 8)
//
//     } catch (err) {
//         console.log(err)
//         throw err
//     }
// }
