import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {MapContainer, Marker, Popup, useMap} from "react-leaflet";
import ReactLeafletGoogleLayer from 'react-leaflet-google-layer';
import {LatLngExpression, LeafletEventHandlerFnMap} from "leaflet";
import {MapDraggableMarkerWrapper} from "./MapDraggableMarker.style";
import {GeoWorksMarkerDict} from "../../../../../type/data/module/GeoWorks/marker.type";

interface MapCustomMarkerProps {
    mapMarker: GeoWorksMarkerDict | null
    zoom: number
    markerIconCallback: (mapMarker: any) => any
}


function ChangeView({ center, zoom }: {center: LatLngExpression, zoom: number}) {
    const map = useMap();
    map.setView(center, zoom);
    return null;
}

export const MapDraggableMarker: React.FC<MapCustomMarkerProps> = ({
                                                                       mapMarker,
                                                                       zoom= 16,
                                                                       markerIconCallback,
                                                                   }) => {

    const [map, setMap] = useState<any>(null);

    const [draggable, setDraggable] = useState(true)
    const [position, setPosition] = useState<LatLngExpression>(mapMarker?.position ? mapMarker.position: [0,0])
    const markerRef = useRef<any>(null)
    const mapRef = useRef<any>(null)

    useEffect(() => {
        if (mapMarker) {
            setPosition(mapMarker.position)
        }
    }, [mapMarker])

    const toggleDraggable = useCallback((draggable) => {
        setDraggable(draggable)
    }, [])

    const eventHandlers = useMemo<LeafletEventHandlerFnMap>(
        () => ({
            mouseover: () => {
                toggleDraggable(true)
            },
            mouseout: () => {
                toggleDraggable(true)
            },
            dragend() {
                const marker = markerRef.current
                if (marker != null) {
                    setPosition(marker.getLatLng())
                }
            },
        }),
        [toggleDraggable],
    )

    const icon = markerIconCallback(mapMarker)

    if (mapMarker) {
        return (
            <MapDraggableMarkerWrapper>
                <MapContainer
                    ref={mapRef}
                    center={position}
                    zoom={zoom}
                    scrollWheelZoom={false}
                    whenCreated={setMap}
                >
                    <ChangeView
                        center={position}
                        zoom={zoom}
                    />
                    <ReactLeafletGoogleLayer
                        apiKey={'AIzaSyAFIrTCVnw2zFiGeUqDwA9rdTEjxe_1Vxk'}
                        type={'hybrid'}
                    />
                    <Marker
                        ref={markerRef}
                        key={mapMarker.id}
                        position={position}
                        icon={icon}
                        draggable={draggable}
                        eventHandlers={eventHandlers}
                    />
                </MapContainer>
            </MapDraggableMarkerWrapper>
        )
    } else {
        return <></>
    }




}

