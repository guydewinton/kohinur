import styled from "styled-components";
import Paragraph from "antd/es/typography/Paragraph";

const StyledParagraph = styled(Paragraph)``

export const ParagraphWidget = StyledParagraph

export type {ParagraphProps} from 'antd/es/typography/Paragraph'