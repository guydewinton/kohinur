import {TypeOrmPropExtendedOptions} from "../../../../../type/options.type";
import {GraphqlField} from "../../../api/gql/decorators/prop/gql.prop.decorator";
import {Column} from "typeorm";
import {GraphQLBoolean} from "graphql";

export const BooleanField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, GraphQLBoolean, options)
        Column('boolean', options)(prototype, propertyName)
    }
}