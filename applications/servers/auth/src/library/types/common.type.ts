
export type GenericObject = {
    [prop: string]: any;
}
