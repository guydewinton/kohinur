import {TypeGqlBaseOptions} from "../../../../../../type/options.type";
import {ObjectType} from "type-graphql";

export const GraphqlModel = (constructor: Function, options?: TypeGqlBaseOptions) => {
    const graphqlOptions = {} as TypeGqlBaseOptions
    options?.name ? graphqlOptions.name = options.name : null;
    options?.description ? graphqlOptions.description = options.description : null;

    if (options?.select !== false) {
        if (options?.array) {
            ObjectType(graphqlOptions)(constructor)
        } else {
            ObjectType(graphqlOptions)(constructor)
        }
    }
}