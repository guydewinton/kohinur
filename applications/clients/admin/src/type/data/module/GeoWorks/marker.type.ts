import {LatLngExpression} from "leaflet";
import {ContactResidentDict} from "./contact.type";
import {JobsDetail} from "./action.type";

export interface GeoWorksMarkerDict {
    id: string
    label: string
    position: LatLngExpression;
    jobs: JobsDetail[];
    access: 'public' | 'private'; // todo: replace with a contact :: as in which people have access to which markers... this might take some thought
    contact: ContactResidentDict;
    note: string;
}

