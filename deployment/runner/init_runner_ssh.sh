. ./runner.env

scp ./runner.env root@${DROPLET_IP}:/
ssh root@${DROPLET_IP} 'bash -s' < ./scripts/runner_provision.sh
