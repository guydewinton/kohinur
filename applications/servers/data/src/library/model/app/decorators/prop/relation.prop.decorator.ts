import {
    GraphqlManyRelation,
    GraphqlSingleRelation
} from "../../../api/gql/decorators/prop/gql.prop.decorator";
import {
    ManyToMany,
    ManyToOne,
    ObjectType,
    OneToMany,
    OneToOne,
    RelationOptions
} from "typeorm";
import {HasuraRelation} from "../../../../action/external/hasura/prop/relations.prop.hasura";

export const OneToOneRelation = <T, K>(relation: () => ObjectType<T>, inverse?: (type: T) => K, options?: RelationOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlSingleRelation(prototype, propertyName, relation(), options)
        OneToOne(relation, inverse, options)(prototype, propertyName)
        HasuraRelation(propertyName, false)
    }
}

export const OneToManyRelation = <T, K>(relation: () => ObjectType<T>, inverse: (type: T) => K, options?: RelationOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlManyRelation(prototype, propertyName, relation(), options)
        OneToMany(relation, inverse, options)(prototype, propertyName)
        HasuraRelation(propertyName, true)
    }
}

export const ManyToOneRelation = <T, K>(relation: () => ObjectType<T>, inverse?: (type: T) => K, options?: RelationOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlSingleRelation(prototype, propertyName, relation(), options)
        ManyToOne(relation, inverse, options)(prototype, propertyName)
        HasuraRelation(propertyName, false)
    }
}

export const ManyToManyRelation = <T, K>(relation: () => ObjectType<T>, inverse?: (type: T) => K, options?: RelationOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlManyRelation(prototype, propertyName, relation(), options)
        ManyToMany(relation, inverse, options)(prototype, propertyName)
        HasuraRelation(propertyName, true)
    }
}
