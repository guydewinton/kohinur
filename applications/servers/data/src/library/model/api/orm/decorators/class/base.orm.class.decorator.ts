import {Entity} from "typeorm";
import {sqlTriggerList} from "../../../sql/plpg/triggers/triggerList";
import {refDeleteTrigger, refInsertTrigger} from "../../../sql/plpg/triggers/refTriggers";
import {ModelOptions} from "../../../../../../type/options.type";

export const PrimaryEntity = (name: string | undefined, options: ModelOptions, constructor: Function) => {

    sqlTriggerList.push(refInsertTrigger(constructor))
    sqlTriggerList.push(refDeleteTrigger(constructor))

    Entity(name, options)(constructor)

}

export const SecondaryEntity = (name: string | undefined, options: ModelOptions, constructor: Function) => {
    Entity(name, options)(constructor)
}

