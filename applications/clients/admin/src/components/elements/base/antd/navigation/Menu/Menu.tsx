import styled from "styled-components";
import Menu from "antd/es/menu";
import 'antd/es/menu/style'

const StyledMenu = styled(Menu)``

export const MenuWidget = StyledMenu

export type {MenuProps} from 'antd/es/menu'

