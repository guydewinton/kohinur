import styled from "styled-components";

interface IconWrapperProps {
    height?: number
    width?: number
    top?: number
    left?: number
}

export const IconWrapper = styled.div<IconWrapperProps>`
  height: ${props => props.height ? `${props.height}px` : '100px'};
  width: ${props => props.width ? `${props.width}px` : props.height ? `${props.height}px` : '100px'};
  display: flex;
  position: relative;
  top: ${props => props.top ? `${props.top}px` : 0};
  left: ${props => props.left ? `${props.left}px` : 0}
`
