import styled from "styled-components";

export const MainWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
`

export const MarkerPaneWrapper = styled.div`
  height: 100%;
  width: 350px;
`

export const MapPaneWrapper = styled.div`
  height: 100%;
  width: 100%;
  background-color: #282c34;
`

export const JobDetailModalWrapper = styled.div`
    
`

export const JobAddNewModalWrapper = styled.div`
    
`