import {GeoWorksMarkerDict} from "../../../../type/data/module/GeoWorks/marker.type";
import {CheckboxOptions} from "../../../../components/elements/widgets/checkboxes/Checkbox.type";

export const useMarkerFilter = (
    baseMarkerArray: GeoWorksMarkerDict[],
    masterFilterToggleContext: CheckboxOptions[][],
    jobCheckboxGridContext: CheckboxOptions[][],
    statusCheckboxGridContext: CheckboxOptions[][],
    searchString: string
) => {
    let reducedMarkers = baseMarkerArray.filter((mapMarker) => {
        let jobFilterBool = false
        let statusFilterBool = false
        if (mapMarker.jobs.length > 0) {
            mapMarker.jobs.forEach((markerJob) => {
                if (masterFilterToggleContext[0][0].checked) {
                    jobCheckboxGridContext.forEach((checkboxRow) => {
                        checkboxRow.forEach((checkboxItem) => {
                            if (checkboxItem.checked) {
                                if (checkboxItem.value === markerJob.typeValue) {
                                    jobFilterBool = true
                                }
                            }
                        })

                    })
                } else {
                    jobFilterBool = true
                }
                if (masterFilterToggleContext[0][1].checked) {
                    statusCheckboxGridContext.forEach((checkboxRow) => {
                        checkboxRow.forEach((checkboxItem) => {
                            if (checkboxItem.checked) {
                                if (checkboxItem.value === markerJob.status) {
                                    statusFilterBool = true
                                }
                            }
                        })

                    })
                } else {
                    statusFilterBool = true
                }
            })
        } else {
            if (!masterFilterToggleContext[0][0].checked && !masterFilterToggleContext[0][1].checked) {
                jobFilterBool = true
                statusFilterBool = true
            }
        }
        return jobFilterBool && statusFilterBool
    })
    reducedMarkers = reducedMarkers.filter((mapMarker) => {
        return mapMarker.label.toLowerCase().includes(searchString)
    })
    reducedMarkers.sort((a, b) => {
        let higher = a.label.toLowerCase();
        let lower = b.label.toLowerCase();
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    })
    return reducedMarkers
}