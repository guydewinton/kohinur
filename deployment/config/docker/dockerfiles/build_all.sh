docker build -f ./deploy/Dockerfile.dns -t registry.gitlab.com/guydewinton/kohinurhall:dns .
docker build -f ./deploy/Dockerfile.hasura -t registry.gitlab.com/guydewinton/kohinurhall:hasura .
docker build -f ./deploy/Dockerfile.nginx -t registry.gitlab.com/guydewinton/kohinurhall:nginx .
docker build -f ./deploy/Dockerfile.node -t registry.gitlab.com/guydewinton/kohinurhall:node .
docker build -f ./deploy/Dockerfile.postgres -t registry.gitlab.com/guydewinton/kohinurhall:postgres .
docker build -f ./deploy/Dockerfile.openssl -t registry.gitlab.com/guydewinton/kohinurhall:openssl .
docker build -f ./deploy/Dockerfile.vault -t registry.gitlab.com/guydewinton/kohinurhall:vault .
