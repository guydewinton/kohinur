import {useEffect, useState} from "react";
import styled from "styled-components";


const PageWrapper = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`

const FormWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

const FormInput = styled.input`
    margin-bottom: 1rem;
`

const FormButton = styled.button`
    width: 100%;
`

export default function Login({redirect}) {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const handleChangeEmail = (event) => {
        setEmail(event.target.value)
    }

    const handleChangePassword = (event) => {
        setPassword(event.target.value)
    }

    const useSubmitLogin = async () => {

        const body = JSON.stringify({
            email: email,
            password: password,
            redirect: redirect
        })
        const response = await fetch('https://auth.mullumfoodcoop.com/v1/auth/user/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            credentials: 'include',
            body: body,

        })
        if (response.status === 201) {
            if (redirect) {
                window.location.href = redirect;
            } else if (process.env.DEFAULT_REDIRECT_ROUTE) {
                window.location.href = process.env.DEFAULT_REDIRECT_ROUTE;
            }

        } else {
            console.log('nnoooo!!!')
        }

    }

    return (
        <PageWrapper>
            <FormWrapper>
                <FormInput value={email} onChange={handleChangeEmail}/>
                <FormInput type={'password'} value={password} onChange={handleChangePassword}/>
                <FormButton onClick={useSubmitLogin}>Login</FormButton>
            </FormWrapper>
        </PageWrapper>
    )
}

export async function getServerSideProps(context) {
    const redirect = context.req.cookies['mfc-login-redirect']
    context.res.setHeader("Set-Cookie", `mfc-login-redirect=delete; Max-Age=-1; Domain=.mullumfoodcoop.com; Path=/`)
  return {

    props: {
        redirect: redirect
    }
  }
}