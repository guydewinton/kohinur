import {createParamDecorator} from "type-graphql";

const IsLive = () => {
    return createParamDecorator(async ({info}): Promise<boolean> => {
        let isLive: boolean = false;
        info.operation.directives?.forEach((val) => {
            val.arguments?.forEach((arg) => {
                console.log(arg)
                // todo: decide if I want to use if and throttle args??? maybe return a number
            })
            if (val.name.value === 'live') {
                isLive = true
            }
        })
        return isLive;
    });
};

export default IsLive;