import {Router} from "express";
import parseRouteAccessGroupsHeader from "../../library/middleware/parse/parseRouteAccessGroupsHeader";
import parseRefreshTokenCookie from "../../library/middleware/parse/parseRefreshTokenCookie";
import authRefreshToken from "../../library/middleware/auth/authRefreshToken";
import jwt from "../../library/helpers/crypto/token/jwt/jwt";
import {Group} from "../../schema/Group.model";
import {cookieOptions} from "../../library/token/cookie";
import transposeRefererHeader from "../../library/middleware/headers/login";


const refreshRouter = Router();

refreshRouter.get(
    '/validate',
    transposeRefererHeader,
    parseRouteAccessGroupsHeader,
    parseRefreshTokenCookie,
    authRefreshToken,
    async (_req, res) => {

        if (res.locals.status === 201) {
                const refreshToken = jwt.generateRefreshToken(res.locals.user.id);
                res.locals.userGroupList = res.locals.user.groups.map((group: Group) => group.group)
                res.cookie('mfc', refreshToken, cookieOptions);
                return res.status(201).send()
        }

        return res.status(401).send('Refresh Token Validation Failed')



});

export default refreshRouter;