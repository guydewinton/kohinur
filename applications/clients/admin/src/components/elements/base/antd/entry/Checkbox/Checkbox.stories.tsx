import { CheckboxWidget } from "./Checkbox";
import {useState} from "react";

export default {
  title: "Base / Antd / Checkbox",
};
export const CheckboxBasic = () => {

  const [checked, setChecked] = useState<boolean>(false)

  const handleCheck = () => {
    setChecked(prev => !prev)
  }

  return (
      <CheckboxWidget
          onChange={handleCheck}
          disabled={false}
          checked={checked}
      >
        {'Checkbox Label'}
      </CheckboxWidget>
  )
};
CheckboxBasic.storyName = "Basic Checkbox";
