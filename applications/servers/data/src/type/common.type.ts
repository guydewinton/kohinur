
export interface GenericObject {
    [prop: string]: any;
}

export interface NamedObject {
    name: string;
    [prop: string]: any;
}