import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {MapContainer, Marker, Popup, useMap} from "react-leaflet";
import ReactLeafletGoogleLayer from 'react-leaflet-google-layer';
import {LatLngExpression, LeafletEventHandlerFnMap} from "leaflet";
import {MapDraggableMarkerWrapper} from "./MapDraggableMarker.style";

interface MapCustomMarkerProps {
    center: LatLngExpression | null
    zoom: number
    markerIconCallback: () => any
    dragCallback: (position: LatLngExpression) => void
    markerId: string | number
}



function ChangeView({ center, zoom }: {center: LatLngExpression, zoom: number}) {
    const map = useMap();
    map.setView(center, zoom);
    return null;
}

export const MapDraggableMarker: React.FC<MapCustomMarkerProps> = ({
                                                                    center,
                                                                    zoom= 16,
                                                                    markerIconCallback,
                                                                    dragCallback,
                                                                       markerId
                                                                }) => {

    const [map, setMap] = useState<any>(null);

    const [draggable, setDraggable] = useState(true)
    const [position, setPosition] = useState<LatLngExpression>(center ? center: [0,0])
    const markerRef = useRef<any>(null)

    useEffect(() => {
        if (center) {
            setPosition(center)
        }
    }, [center])

    const toggleDraggable = useCallback((draggable) => {
        setDraggable(draggable)
    }, [])

    const eventHandlers = useMemo<LeafletEventHandlerFnMap>(
        () => ({
            mouseover: () => {
                toggleDraggable(true)
            },
            mouseout: () => {
                toggleDraggable(true)
            },
            dragend() {
                const marker = markerRef.current
                if (marker != null) {
                    setPosition(marker.getLatLng())
                    dragCallback(marker.getLatLng())
                }
            },
        }),
        [toggleDraggable],
    )

    const icon = markerIconCallback()

    if (markerId) {
        return (
            <MapDraggableMarkerWrapper>
                <MapContainer
                    center={position}
                    zoom={zoom}
                    scrollWheelZoom={false}
                    whenCreated={setMap}
                >
                    <ChangeView
                        center={position}
                        zoom={zoom}
                    />
                    <ReactLeafletGoogleLayer
                        // apiKey={process.env.googleAPI}
                        type={'hybrid'}
                    />


                    <Marker
                        ref={markerRef}
                        position={position}
                        icon={icon}
                        draggable={draggable}
                        eventHandlers={eventHandlers}
                        key={markerId}
                    />
                </MapContainer>
            </MapDraggableMarkerWrapper>
        )
    } else {
        return <></>
    }







}