const { merge } = require('webpack-merge')
const commonConfig = require('./webpack.shared.js')

module.exports = (envVars) => {
    const { mode } = envVars
    const envConfig = require(`./webpack.${mode}.js`)
    return merge(commonConfig, envConfig)
}
