import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {markerArray} from "../data/markers";
import {GeoWorksMarkerDict} from "../../../../type/data/module/GeoWorks/marker.type";

const markerSlice = createSlice({
    name: 'marker',
    initialState: {value: markerArray},
    reducers: {
        createMarker: (state, action: PayloadAction<GeoWorksMarkerDict>) => {
            state.value.push(action.payload)
        },
        updateMarker: (state, action: PayloadAction<GeoWorksMarkerDict>) => {
                state.value = state.value.map(obj => {
                    if (action.payload.id === obj.id) {
                        return action.payload
                    } else {
                        return obj
                    }
                })

        },
        deleteMarker: (state, action: PayloadAction<string>) => {
            state.value = state.value.filter(marker => {
                return action.payload !== marker.id
            })
        }
    }
})

export const {
    createMarker,
    updateMarker,
    deleteMarker
} = markerSlice.actions

export default markerSlice.reducer