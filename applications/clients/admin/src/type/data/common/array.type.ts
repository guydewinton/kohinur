import {DataObject, GenericObject} from "./object.type";

export type GenericArray = any[]

export type GenericNestedArray = any[][]

export type GenericObjectArray = GenericObject[]

export type ArrayElement<T extends GenericArray> = T extends Array<infer U> ? U : GenericObject

export type DataObjectArray = DataObject[]

export type GridIndex = [number, number]