import React, {ChangeEvent, useContext, useEffect, useRef, useState} from 'react';
import {
    BaseOptionType,
    DefaultOptionType,
    SelectOptions,
    SelectWidget
} from "../../../../elements/base/antd/entry/Select/Select";
import {
    JobsDetail,
    JobTypeLabelUnion,
    JobTypeValueUnion
} from "../../../../../type/data/module/GeoWorks/action.type";
import {v4} from "uuid";
import {InputRef, InputWidget} from "../../../../elements/base/antd/entry/Input/Input"
import { ModalWidget } from '../../../../elements/base/antd/window/Modal/Modal';
import { ButtonWidget } from '../../../../elements/base/antd/general/Button/Button';
import { FormWidget } from '../../../../elements/base/antd/entry/Form/Form';
import {jobCheckboxGrid} from "../../../../../library/data/GeoWorks/GeoWorks.data";

interface JobEditModalProps {
    job: JobsDetail | null
    onSave: (job: JobsDetail) => void
    onCancel: () => void
    onDelete?: (id: string) => void
}

const JobEditDialog: React.FC<JobEditModalProps> = ({
        job,
        onSave,
        onCancel,
        onDelete
    }) => {

    const [jobDescriptionValue, setJobDescriptionValue] = useState<string>(job?.issue ? job.issue : '')
    const [jobTypeValue, setJobTypeValue] = useState<string>(job?.typeValue ? job?.typeValue : '')
    const [jobTypeLabel, setJobTypeLabel] = useState<string>(job?.typeLabel ? job?.typeLabel : '')
    const [inputStatus, setInputStatus] = useState<'' | 'error'>('')
    const [jobSet, setJobSet] = useState<SelectOptions[]>([])
    const initInput = useRef<InputRef>(null)
    const pageLoaded = useRef<boolean>(false)

    useEffect(() => {
        if (!pageLoaded.current && initInput.current) {
            initInput.current.focus({
                cursor: 'start',
            })
            pageLoaded.current = true
        }
    })

    useEffect(() => {
        if (job) {
            setJobDescriptionValue(job.issue)
            setJobTypeValue(job.typeValue)
        } else {
            setJobDescriptionValue('')
            setJobTypeValue('')
        }

    }, [job])

    useEffect(() => {
        const newJobType = jobSet.find((job) => {return job.value === jobTypeValue})
        setJobTypeLabel(newJobType?.label && typeof newJobType?.label === 'string' ? newJobType.label : '')
    }, [jobTypeValue, jobSet])

    useEffect(() => {
        const jobTypes: SelectOptions[] = []
        jobCheckboxGrid.forEach(jobTypeOne => {
            jobTypeOne.forEach(jobTypeTwo => {
                jobTypes.push({...jobTypeTwo, key: jobTypeTwo.value, disabled: false})
            })
        })
        setJobSet(jobTypes)
    }, [])

    const handleOk = () => {
        onSave({
            id: job ? job.id : v4(),
            typeValue: jobTypeValue as JobTypeValueUnion,
            typeLabel: jobTypeLabel as JobTypeLabelUnion,
            issue: jobDescriptionValue,
            status: 'pending',
        })
    };

    const handleCancel = () => {
        setJobDescriptionValue('')
        onCancel()
    };

    const handleMarkerLabelChange = (event: ChangeEvent<HTMLInputElement>) => {
        setJobDescriptionValue(event.target.value)
        inputStatus === 'error' && setInputStatus('')
    }

    const handleSelect = (value: unknown, option: DefaultOptionType | BaseOptionType | (DefaultOptionType | BaseOptionType)[]) => {
        if (typeof value === "string") {
            setJobTypeValue(value ? value : '')
        }
    }

    return (
        <>
            <ModalWidget title={`${job ? 'Edit' : 'Add New'} Job`} visible={true} onOk={handleOk} onCancel={handleCancel}
                footer={[
                   (job && onDelete &&
                        <ButtonWidget key="delete" type="primary" onClick={() => onDelete(job?.id)} danger>
                            Delete
                        </ButtonWidget>
                   ),
                   <ButtonWidget key="cancel" onClick={handleCancel}>
                       Cancel
                   </ButtonWidget>,
                   <ButtonWidget key="save" type="primary" onClick={handleOk} >
                       Save
                   </ButtonWidget>,

                ]}
            >
                <FormWidget labelCol={{span: 5}} wrapperCol={{span: 17}}>
                    <FormWidget.Item label={'Label'}>
                        <InputWidget
                            ref={initInput}
                            value={jobDescriptionValue}
                            onChange={handleMarkerLabelChange}
                            status={inputStatus}
                            onKeyPress={(event) => {
                                if (event.key === "Enter") {
                                    handleOk()
                                }
                            }}
                        />
                    </FormWidget.Item>
                    <FormWidget.Item label={'Type'}>
                        <SelectWidget
                            options={jobSet}
                            value={jobTypeValue}
                            onChange={handleSelect}
                        />
                    </FormWidget.Item>
                </FormWidget>
            </ModalWidget>
        </>
    );
};

export default JobEditDialog;