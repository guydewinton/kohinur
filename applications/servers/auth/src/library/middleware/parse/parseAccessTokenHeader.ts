import {NextFunction, Request, Response} from "express";
import {decode} from "jsonwebtoken";
import {AccessTokenBody, DecodedAccessTokenBody} from "../../types/token.type";
import {accessBodyGuard} from "../../helpers/crypto/token/jwt/defaults/jwtDecoder";

export const parseAccessTokenHeader = (req: Request, res: Response, next: NextFunction) => {

    let accessToken = req.header('authorization')?.split(' ')[1];

    if (!accessToken) {
        console.log('Access Token Undefined')
        return res.status(401).send('Access Token Undefined');
    }

    const tokenBody = decode(accessToken) as DecodedAccessTokenBody

    if (!tokenBody || typeof tokenBody === 'string' || !accessBodyGuard(tokenBody)) {
        console.log('Invalid Token Body')
        return res.status(401).send('Invalid Token Body');
    }

    res.locals.accessToken = accessToken as string;
    res.locals.accessTokenBody = tokenBody as AccessTokenBody;

    return next()

}

export default parseAccessTokenHeader;