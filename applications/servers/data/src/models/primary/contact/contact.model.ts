import {Address} from "./secondary/address.contact.model";
import {ContactPoint} from "./secondary/point.contact.model";
import {PrimaryBaseModel} from "../../abstract/primary.base.model";
import {ContactSet} from "./contact.set.model";
import {VarCharField} from "../../../library/model/app/decorators/prop/character.prop.decorator";
import {IntegerField} from "../../../library/model/app/decorators/prop/numeric.prop.decorator";
import {
    ManyToOneRelation,
    OneToManyRelation
} from "../../../library/model/app/decorators/prop/relation.prop.decorator";
import PrimaryModel from "../../../library/model/app/decorators/class/PrimaryModel.class.decorator";


@PrimaryModel()
export class Contact extends PrimaryBaseModel {

    @VarCharField()
    name: string;

    @IntegerField()
    rank: number;

    @OneToManyRelation(() => Address, address => address.contact)
    addresses: Address[];

    @OneToManyRelation(() => ContactPoint, contactPoint => contactPoint.contact)
    contactPoints: ContactPoint[];

    @ManyToOneRelation(() => ContactSet)
    contactSet: ContactSet;


}
