import styled from "styled-components";
import {Divider} from "antd/es";
import 'antd/es/divider/style'

const StyledDivider = styled(Divider)`
`

export const DividerWidget = StyledDivider

export type {DividerProps} from 'antd/es/divider'