import { v4 } from 'uuid';
import {CheckboxOptions} from "../../../../components/elements/widgets/checkboxes/Checkbox.type";

import {GeoWorksMarkerDict} from "../../../../type/data/module/GeoWorks/marker.type";


export const markerArray: GeoWorksMarkerDict[] = [
    {
        id: v4(),
        label: 'Kohinur Hall',
        position: [-28.4856, 153.4103],
        access: 'public',
        note: '',

        jobs: [
            {
                id: v4(),
                typeValue: 'gas',
                typeLabel: 'Gas',
                issue: 'Lots of gas',
                status: 'pending',
            }
        ],
        contact: {
            type: 'resident',
            alias: 'Bob',
            givenName: 'Bob',
            surname: 'Harris',
        }
    },
    {
        id: v4(),
        label: '1234 Main Arm Rd',
        position: [-28.4838, 153.4203],
        access: 'public',
        jobs: [],
        note: '',
        contact: {
            type: 'resident',
            alias: 'Bob',
            surname: 'Harris',

        }
    },
    {
        id: v4(),
        label: '2641 Main Arm Rd',
        position: [-28.4924, 153.4003],
        access: 'public',
        note: '',
        jobs: [

        ],
        contact: {
            type: 'resident',
            alias: 'Bob',
            givenName: 'Bob',
            surname: 'Harris',

        }
    },
    {
        id: v4(),
        label: '1362 Main Arm Rd',
        position: [-28, 153],
        access: 'public',
        jobs: [],
        note: '',
        contact: {
            type: 'resident',
            alias: 'Bob',
            givenName: 'Bob',
            surname: 'Harris',
        }
    }
]

