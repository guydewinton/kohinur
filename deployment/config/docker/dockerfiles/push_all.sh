docker push registry.gitlab.com/guydewinton/kohinurhall:dns
docker push registry.gitlab.com/guydewinton/kohinurhall:hasura
docker push registry.gitlab.com/guydewinton/kohinurhall:nginx
docker push registry.gitlab.com/guydewinton/kohinurhall:node
docker push registry.gitlab.com/guydewinton/kohinurhall:postgres
docker push registry.gitlab.com/guydewinton/kohinurhall:openssl
docker push registry.gitlab.com/guydewinton/kohinurhall:vault
