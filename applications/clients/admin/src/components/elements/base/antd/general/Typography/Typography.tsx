import Typography from "antd/es/typography";
import styled from "styled-components";
import 'antd/es/typography/style'

const StyledTypography = styled(Typography)``

export const TypographyWidget = StyledTypography

export type {TypographyProps} from 'antd/es/typography/'