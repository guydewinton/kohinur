import styled from "styled-components";
import Text from "antd/es/typography/Text";

const StyledText = styled(Text)``

export const TextWidget = StyledText

export type {TextProps} from 'antd/es/typography/Text'
