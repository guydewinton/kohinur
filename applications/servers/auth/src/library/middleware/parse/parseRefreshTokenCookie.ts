import {NextFunction, Request, Response} from "express";
import {decode} from "jsonwebtoken";
import {DecodedRefreshTokenBody, RefreshTokenBody} from "../../types/token.type";
import {accessBodyGuard} from "../../helpers/crypto/token/jwt/defaults/jwtDecoder";

export const parseRefreshTokenCookie = (req: Request, res: Response, next: NextFunction) => {

    const refreshToken = req.cookies.mfc;

    console.log('cookies', req.cookies)

    if (!refreshToken) {
        console.log('Refresh Token Undefined')
        return res.status(401).send('Refresh Token Undefined');
    }
    const tokenBody = decode(refreshToken) as DecodedRefreshTokenBody

    if (!tokenBody || typeof tokenBody === 'string' || !accessBodyGuard(tokenBody)) {
        console.log('Invalid Refresh Token Body')
        return res.status(401).send('Invalid Refresh Token Body');
    }

    res.locals.refreshToken = refreshToken as string;
    res.locals.refreshTokenBody = tokenBody as RefreshTokenBody

    return next();

}

export default parseRefreshTokenCookie;