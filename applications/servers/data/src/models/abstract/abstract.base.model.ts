import {BaseEntity} from "typeorm";
import {
    CreatedDateTimeField,
    ModifiedDateTimeField
} from "../../library/model/app/decorators/prop/datetime.prop.decorator";

export abstract class AbstractBaseModel extends BaseEntity {

    @CreatedDateTimeField()
    dateCreated: Date;

    @ModifiedDateTimeField()
    dateUpdated: Date;

}