import axios from "axios";

export const createObjectRelation = async(localTableName: string, localForeignKeyName: string, localForeignKeyColumn: string) => {
    await axios('http://hasura-data:8080/v1/metadata',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Hasura-Role': 'admin'
        },
        data: {
            "type": "pg_create_object_relationship",
            "args": {
                "table": localTableName,
                "name": localForeignKeyName,
                "source": "data",
                "using": {
                    "foreign_key_constraint_on" : [localForeignKeyColumn]
        }
            }
        }
    }).catch(function (error) {
        console.log('Error', error.message);
        console.log(error.response.data)
    })
}

export const createArrayRelation = async(localTableName: string, localForeignKeyName: string, inverseTableName: string, inverseForeignKeyColumn: string) => {
    await axios('http://hasura-data:8080/v1/metadata',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Hasura-Role': 'admin'
        },
        data: {
            "type": "pg_create_array_relationship",
            "args": {
                "source": "data",
                "table": localTableName,
                "name": localForeignKeyName,
                "using": {
                    "manual_configuration" : {
                        "remote_table" : inverseTableName,
                        "column_mapping" : {
                            "id" : inverseForeignKeyColumn
                }
            }
        }
            }
        }
    }).catch(function (error) {
        console.log('Error', error.message);
        console.log(error.response.data)
    })
}
