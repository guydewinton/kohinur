import styled from "styled-components";

import {Table} from 'antd/es';
import 'antd/es/table/style'

const StyledTable = styled(Table)`
  height: 100%;

  .ant-table-wrapper {
    height: 100%;

    .ant-spin-nested-loading {
      height: 100%;

      .ant-spin-container {
        height: 100%;
        display: flex;
        flex-flow: column nowrap;

        .ant-table {
          flex: auto;
          height: 100%;
          overflow: hidden;

          .ant-table-container {
            height: 100%;
            display: flex;
            flex-flow: column nowrap;

            .ant-table-header {
              flex: none;
            }

            .ant-table-body {
              flex: auto;
              overflow: scroll;
            }
          }
        }

        .ant-table-pagination {
          flex: none;
        }
      }
    }
  }
  thead {
    

    th {
      font-weight: bold;
    }
  }

  
`

export const TableWidget = StyledTable

export type { TableProps, TablePaginationConfig, ColumnProps, ColumnsType, ColumnType, ColumnGroupType } from 'antd/es/table'