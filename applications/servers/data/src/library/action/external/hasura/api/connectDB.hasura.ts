import axios from "axios";

export const connectHasuraDB = async() => {
    await axios('http://hasura-data:8080/v1/metadata',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Hasura-Role': 'admin'
        },
        data: JSON.stringify({
            "type": "pg_add_source",
            "args": {
                "name": 'data',
                "configuration": {
                    "connection_info": {
                        "database_url":  "postgresql://mullumfoodcoop:mullum4ever@postgres-data:5432/data",
                        "pool_settings": {
                            "retries": 1,
                            "idle_timeout": 180,
                            "max_connections": 50
                        }
                    }
                }
            }
        })
    }).catch(function (error) {
        console.log('Error', error.message);
        console.log(error.response.data)
    })
}




