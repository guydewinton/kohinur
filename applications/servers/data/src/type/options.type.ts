import {EntityOptions} from "typeorm/decorator/options/EntityOptions";

export interface TypeGqlBaseOptions {
    name?: string;
    description?: string;
    [prop: string]: any;
}

export interface TypeOrmPropBaseOptions {
    name?: string;
    comment?: string;
}

export interface TypeOrmPropExtendedOptions extends TypeOrmPropBaseOptions{
    update?: boolean;
    insert?: boolean;
    select?: boolean;
    default?: number;
    nullable?: boolean;
    array?: boolean;
}


export interface TypeOrmFloatPropOptions extends TypeOrmPropExtendedOptions {
    precision?: number;
    scale?: number;
}

export interface TypeOrmVarCharPropOptions extends TypeOrmPropExtendedOptions {
    length?: number;
}

export interface ModelOptions extends EntityOptions {
    description?: string;
}
