import {PrimaryBaseModel} from "../../abstract/primary.base.model";
import {Contact} from "./contact.model";
import {Contactable} from "../../polymorphic/Contactable.model";
import {
    ManyToOneRelation,
    OneToManyRelation
} from "../../../library/model/app/decorators/prop/relation.prop.decorator";
import PrimaryModel from "../../../library/model/app/decorators/class/PrimaryModel.class.decorator";

@PrimaryModel()
export class ContactSet extends PrimaryBaseModel {

    @OneToManyRelation(() => Contact, contact => contact.contactSet)
    contacts: Contact[];
    
    @ManyToOneRelation(() => Contactable)
    contactable: Contactable;


}