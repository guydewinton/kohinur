import styled from "styled-components";
import Space from "antd/es/space";
import 'antd/es/space/style'

const StyledSpace = styled(Space)

export const SpaceWidget = StyledSpace

export type {SpaceProps} from 'antd/es/space'