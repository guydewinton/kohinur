import React, {useCallback, useEffect, useState} from "react";
import {MapContainer, Marker, ZoomControl} from "react-leaflet";
import ReactLeafletGoogleLayer from 'react-leaflet-google-layer';
import {LatLngExpression, Map as LeafletMap, control} from "leaflet";
import {useLeafletContext} from '@react-leaflet/core';
import {GeoWorksMarkerDict} from "../../../../../type/data/module/GeoWorks/marker.type";
import {MapCustomMarkerWrapper, StyledAddMarkerButton, StyledAddMarkerButtonWrapper, StyledPopup} from "./MapCustomMarker.style";

interface MapCustomMarkerProps {
    markerArray: GeoWorksMarkerDict[]
    onAddNewMarker: (markerPosition: LatLngExpression) => void
    center: LatLngExpression
    zoom: number
    markerIconCallback: (mapMarker: any) => any
    MarkerPopupContent: React.FC<{mapMarker: GeoWorksMarkerDict}>
    addingNewMarker: boolean
}

export const MapCustomMarker: React.FC<MapCustomMarkerProps> = ({
            markerArray = [],
            onAddNewMarker,
            center= [0, 0],
            zoom= 16,
            markerIconCallback,
            MarkerPopupContent,
            addingNewMarker
        }) => {

    const [map, setMap] = useState<LeafletMap | null>(null)
    const [mapLoadState, setMapLoadState] = useState<0|1|2>(0);
    const [addMarker, setAddMarker] = useState<{
        position: LatLngExpression,
        display: boolean
    }>({
        position: center,
        display: false
    });

    const [addNewMarkerLoader, setAddNewMarkerLoader] = useState<boolean>(false)
    const [mouseOverMarker, setMouseOverMarker] = useState(false)

    const handleMapClick = useCallback((e: any) => {
        if (!mouseOverMarker) {
            setAddMarker((prev) => {
                return {
                    position: [e.latlng.lat, e.latlng.lng],
                    display: !prev.display
                }
            });
        }
    }, [mouseOverMarker])

    useEffect(() => {
        if (mapLoadState === 1 && map) {
            setMapLoadState(2)
            map.addEventListener("click", (e: any) => {
                handleMapClick(e)
            });
        }
    }, [mapLoadState, map, handleMapClick])

    const handleMapLoaded = (mapInstance: LeafletMap) => {
        setMap(mapInstance)
        setMapLoadState(1)
    }

    useEffect(() => {

        setAddMarker(prev => {
            return {
                position: addingNewMarker ? prev.position : center,
                display: addingNewMarker
            }

        });
        setAddNewMarkerLoader(addingNewMarker)
    }, [addingNewMarker, setAddMarker, setAddNewMarkerLoader, center])

    const handleAddNewMarker = (markerPosition: LatLngExpression) => {
        onAddNewMarker(markerPosition)
    }

    return (
        <MapCustomMarkerWrapper>
            <MapContainer
                center={center}
                zoom={zoom}
                scrollWheelZoom={false}
                whenCreated={handleMapLoaded}
                zoomControl={false}
            >
                <ZoomControl position="topright" />
                {
                    markerArray.map((mapMarker, index) => {
                        const icon = markerIconCallback(mapMarker)
                        return (
                            <Marker key={mapMarker.id} position={mapMarker.position} icon={icon} eventHandlers={{
                                click: () => {
                                    setMouseOverMarker(false)
                                },
                                mouseover: () => {
                                    setMouseOverMarker(true)
                                },
                                mouseout: () => {
                                    setMouseOverMarker(false)
                                },
                            }}>
                                <StyledPopup closeOnEscapeKey>
                                    <MarkerPopupContent mapMarker={mapMarker}/>
                                </StyledPopup>
                            </Marker>
                        )
                    })
                }
                <ReactLeafletGoogleLayer
                    apiKey={process.env.googleAPI}
                    type={'hybrid'}
                />

                {addMarker.display ? <Marker position={addMarker.position}/> : <></>}
            </MapContainer>
            {addMarker.display ? (
                <StyledAddMarkerButtonWrapper>
                    <StyledAddMarkerButton
                        onClick={() => handleAddNewMarker(addMarker.position)}
                        loading={addingNewMarker}
                    >
                        <p>Add New Marker</p>
                    </StyledAddMarkerButton>
                </StyledAddMarkerButtonWrapper>
            ) : <></>}
        </MapCustomMarkerWrapper>
    )


}


