import {Router} from "express";

import {User} from "../../schema/User.model";
import jwt from "../../library/helpers/crypto/token/jwt/jwt";
import {cookieOptions} from "../../library/token/cookie";
import {verifyHash} from "../../library/helpers/crypto/hashing/hash";


const authRouter = Router();


authRouter.post('/user/login', async (req, res) => {

    console.log(req.headers)
    console.log(req.body)

    const email = req.body.email;
    const password = req.body.password;

    let user;
    try {
        user = await User.findOne({where: {email}});
    } catch (err) {
        return res.status(400).send();
    }

    if (!user) {
        return res.status(401).send();
    }

    if (user.status === 'INACTIVE' || user.status === 'PENDING') {
        return res.status(401).send();
    }

    if (!await verifyHash(password, user.password)) {
        res.status(401);
        return res.send();
    }

    user.refreshToken = jwt.generateRefreshToken(user.id);

    try {
        await user.save()
    } catch (err) {
        return res.status(500).send(err)
    }



    res.cookie('mfc', user.refreshToken, cookieOptions)

    return res.status(201).send()

});


authRouter.post('/logout', async (_req, _res) => {

});


export default authRouter;