export interface NodeInterface<DataModel> {
    id: string;
    data: DataModel
}

export enum ContactPointType {
    PHONE = 'phone',
    EMAIL = 'email',
    SOCIAL = 'social',
}

export enum ContactSuperTypeEnum {
    PERSON = 'PERSON',
    COMPANY = 'COMPANY'
}

