import {UserStatus} from "../../schema/User.model";
import {UserGroups} from "../../schema/Group.model";

export interface AccessTokenBody {
    userId: string;
    status: UserStatus;
    groups: UserGroups[];
}

export interface RefreshTokenBody {
    userId: string;
    status: UserStatus;
    groups: UserGroups[];
}

export type DecodedAccessTokenBody = AccessTokenBody | string;

export type DecodedRefreshTokenBody = RefreshTokenBody | string;

// export interface CookieOptions {
//     httpOnly: boolean
//     domain: string
//     sameSite: string
//     secure: boolean
//     expires: Date
//     maxAge: number
// }