import {Field, ObjectType, createUnionType} from "type-graphql";

@ObjectType()
export class StatusMessage {
    @Field()
    success: boolean;
    @Field()
    status: number;
    @Field()
    message: string;
    constructor(success: boolean, status: number, message: string) {
        this.success = success;
        this.status = status;
        this.message = message;
    }
}

@ObjectType()
export class LoginToken {
    @Field()
    access: string;
    @Field()
    refresh: string;
    constructor(access: string, refresh: string) {
        this.access = access
        this.refresh = refresh
    }
}

@ObjectType()
export class StatusToken {
    @Field()
    success: boolean;
    @Field()
    status: number;
    @Field(() => LoginToken)
    token: LoginToken;
    constructor(success: boolean, status: number, token: LoginToken) {
        this.success = success;
        this.status = status;
        this.token = token;
    }
}

export const LoginResponseUnion = createUnionType({
    name: 'LoginResponseUnion',
    types: () => [StatusMessage, StatusToken] as const
})