import {Router} from "express";

const userRouter = Router();

userRouter.get('/test', (_req, res) => {
    return res.send('hello')
});

export default userRouter;