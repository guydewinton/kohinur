require('dotenv').config()
const fs = require('fs')

module.exports = {
   "type": "postgres",
   "name": "default",
   "host": process.env.POSTGRES_HOST,
   "port": process.env.POSTGRES_PORT,
   "username": process.env.POSTGRES_USER,
   "password": fs.readFileSync(process.env.POSTGRES_PASSWORD_FILE).toString(),
   "database": process.env.POSTGRES_DB,
   "synchronize": true,
   "logging": false,
   "entities": [
      __dirname + "/src/models/**/*.model{.ts,.js}"
   ],
   "migrations": [
      "migrations/**/*.ts"
   ],
   "subscribers": [
      "src/subscribers/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "src/schema/",
      "migrationsDir": "migrations",
      "subscribersDir": "src/subscribers"
   }
}