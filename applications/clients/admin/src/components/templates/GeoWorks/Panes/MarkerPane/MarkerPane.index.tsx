import React, {
    ChangeEvent,
    Dispatch,
    MouseEvent, SetStateAction,
    useContext,
    useEffect,
    useLayoutEffect,
    useRef,
    useState
} from "react";
import {SearchWidget} from "../../../../elements/widgets/input/Search/Search";
import {CheckboxGridWidget} from "../../../../elements/widgets/checkboxes/CheckboxGrid/CheckboxGrid";
import {TableWidget} from "../../../../elements/base/antd/display/Table/Table";
import {GridIndex} from "../../../../../type/data/common/array.type";
import {CheckboxOptions} from "../../../../elements/widgets/checkboxes/Checkbox.type";
import {GeoWorksMarkerDict} from "../../../../../type/data/module/GeoWorks/marker.type";
import {
    MarkerFilterWrapper,
    MarkerJobFilterWrapper,
    MarkerPaneWrapper,
    MarkerTableHeightInner,
    MarkerTableHeightOuter,
    PaddingWrapper,
    VerticalSpace
} from "./MarkerPane.style";
import { CheckboxChangeEvent } from '../../../../elements/base/antd/entry/Checkbox/Checkbox';
import { ButtonWidget } from "../../../../elements/base/antd/general/Button/Button";
import {LatLngExpression} from "leaflet";

const markerTableColumnArray = [
    {
        title: 'Markers',
        dataIndex: 'label',
        key: 0
    }
]

export interface MarkerPaneProps {
    markerArray: GeoWorksMarkerDict[]
    masterFilterToggleSelection: CheckboxOptions[][]
    setMasterFilterToggleSelection: Dispatch<SetStateAction<CheckboxOptions[][]>>
    jobCheckboxGridSelection: CheckboxOptions[][]
    setJobCheckboxGridSelection: Dispatch<SetStateAction<CheckboxOptions[][]>>
    statusCheckboxGridSelection: CheckboxOptions[][]
    setStatusCheckboxGridSelection: Dispatch<SetStateAction<CheckboxOptions[][]>>
    onSelectMarker: (data: GeoWorksMarkerDict) => void
    searchString: string
    setSearchString: Dispatch<SetStateAction<string>>
}

export const MarkerPane: React.FC<MarkerPaneProps> = ({
                                          onSelectMarker,
                                         masterFilterToggleSelection,
                                         setMasterFilterToggleSelection,
                                         jobCheckboxGridSelection,
                                         setJobCheckboxGridSelection,
                                         statusCheckboxGridSelection,
                                         searchString,
                                         setSearchString,
                                         setStatusCheckboxGridSelection,
                                         markerArray
                                     }) => {


    const [tableHeight, setTableHeight] = useState(0)
    const tableWrapperRef = useRef<HTMLDivElement>(null)

    useLayoutEffect(() => {
        if (tableWrapperRef.current) {
            setTableHeight(tableWrapperRef.current.clientHeight - 55)
        }
    }, [tableWrapperRef.current?.clientHeight])

    const handleToggleAllJobCheckboxes = (toggle: boolean) => {
        setJobCheckboxGridSelection((checkboxGrid: CheckboxOptions[][]) => {
            checkboxGrid.forEach((checkboxRow) => {
                checkboxRow.forEach((checkboxItem) => {
                    checkboxItem.checked = toggle
                })
            })
            return [...checkboxGrid]
        })
    }

    const handleTableRowClick = (data: GeoWorksMarkerDict, _index: number | undefined) => {
        return {
            onClick: () => onSelectMarker(data as GeoWorksMarkerDict)

        }
    }

    const handleMapMarkerSearch = (event: ChangeEvent<HTMLInputElement>) => {
        setSearchString(event.target.value)
    }

    const handleMasterCheckboxChange = (gridIndex: GridIndex, event: CheckboxChangeEvent) => {
        setMasterFilterToggleSelection((prev: CheckboxOptions[][]) => {
            prev[gridIndex[0]][gridIndex[1]].checked = event.target.checked
            return [...prev]
        })
    }

    const handleJobCheckboxChange = (gridIndex: GridIndex, event: CheckboxChangeEvent) => {
        setJobCheckboxGridSelection((prev: CheckboxOptions[][]) => {
            prev[gridIndex[0]][gridIndex[1]].checked = event.target.checked
            return [...prev]
        })
    }

    const handleStatusCheckboxChange = (gridIndex: GridIndex, event: CheckboxChangeEvent) => {
        setStatusCheckboxGridSelection((prev: CheckboxOptions[][]) => {
            prev[gridIndex[0]][gridIndex[1]].checked = event.target.checked
            return [...prev]
        })
    }

    return (
        <MarkerPaneWrapper
            className={'MarkerPaneWrapper'}
        >
            <MarkerFilterWrapper
                className={'MarkerFilterWrapper'}
            >
                <VerticalSpace/>
                <SearchWidget
                    placeholder={'Search Map Markers'}
                    value={searchString}
                    onChange={handleMapMarkerSearch}
                />
                <VerticalSpace/>
                <PaddingWrapper>
                    <PaddingWrapper>
                        <CheckboxGridWidget
                            checkboxArray={masterFilterToggleSelection}
                            span={12}
                            onChange={handleMasterCheckboxChange}
                        />
                    </PaddingWrapper>
                </PaddingWrapper>
                <VerticalSpace/>
                <PaddingWrapper>
                    <MarkerJobFilterWrapper>
                        <CheckboxGridWidget
                            checkboxArray={jobCheckboxGridSelection}
                            span={12}
                            onChange={handleJobCheckboxChange}
                        />
                        <VerticalSpace/>
                        <PaddingWrapper>
                            <ButtonWidget
                                onClick={() => handleToggleAllJobCheckboxes(true)}
                                disabled={!masterFilterToggleSelection[0][0].checked}
                            >Select All</ButtonWidget>
                            <ButtonWidget
                                onClick={() => handleToggleAllJobCheckboxes(false)}
                                disabled={!masterFilterToggleSelection[0][0].checked}
                            >Select None</ButtonWidget>
                        </PaddingWrapper>
                    </MarkerJobFilterWrapper>
                </PaddingWrapper>
                <VerticalSpace/>
                <PaddingWrapper>
                    <MarkerJobFilterWrapper>
                        <CheckboxGridWidget
                            checkboxArray={statusCheckboxGridSelection}
                            span={12}
                            onChange={handleStatusCheckboxChange}
                        />
                    </MarkerJobFilterWrapper>
                </PaddingWrapper>
                <VerticalSpace/>
            </MarkerFilterWrapper>
            <MarkerTableHeightOuter
                className={'MarkerTableWrapper'}
            >
                <MarkerTableHeightInner
                    ref={tableWrapperRef}
                    className={'MarkerTableWrapper'}
                >
                    <TableWidget
                        // style={{cursor: 'pointer'}}
                        dataSource={markerArray}
                        columns={markerTableColumnArray}
                        scroll={{y: tableHeight}}
                        onRow={(data, index) => handleTableRowClick(data as GeoWorksMarkerDict, index)}
                        pagination={false}
                        rowKey={'id'}
                    />
                </MarkerTableHeightInner>
            </MarkerTableHeightOuter>
        </MarkerPaneWrapper>
    )
}