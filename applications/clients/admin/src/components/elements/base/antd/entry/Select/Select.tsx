import styled from "styled-components";

import {Select} from "antd/es";
import "antd/es/select/style"
import * as React from "react";

const StyledSelect = styled(Select)`
  width: 100%;
`

export const SelectWidget = StyledSelect

export type {OptionProps, RefSelectProps, BaseOptionType, DefaultOptionType, LabeledValue, SelectValue, InternalSelectProps, SelectProps} from 'antd/es/select'

export interface SelectOptions {
    key: string;
    value: string;
    label: string;
    disabled?: boolean | undefined;
    checked: boolean;
}
