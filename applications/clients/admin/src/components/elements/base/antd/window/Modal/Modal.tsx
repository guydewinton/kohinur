import styled from "styled-components";
import {Modal} from "antd/es";
import "antd/es/modal/style"

const StyledModal = styled(Modal)`
`

export const ModalWidget = StyledModal

export type { ModalProps, ModalFuncProps } from 'antd/es/modal';