import React, {useContext, useEffect, useState} from "react";
import {MarkerPane} from "../../../../components/templates/GeoWorks/Panes/MarkerPane/MarkerPane.index";
import {MapPane} from "../../../../components/templates/GeoWorks/Panes/MapPane/MapPane.index";
import {
    JobDetailModalWrapper,
    MapPaneWrapper,
    MarkerPaneWrapper,
    MainWrapper,
    JobAddNewModalWrapper
} from "./GeoWorks.style";
import {MarkerDetailModal} from "../../../../components/templates/GeoWorks/Modals/MarkerDetailModal/MarkerDetailModal.index";
import MarkerAddNewDialog from "../../../../components/templates/GeoWorks/Dialogs/MarkerAddNewDialog/MarkerAddNewDialog.index";
import {createMarker, deleteMarker, updateMarker} from "../../../../store/features/GeoWorks/slices/markers";
import {v4} from "uuid";
import {CheckboxOptions} from "../../../../components/elements/widgets/checkboxes/Checkbox.type";
import {jobCheckboxGrid, masterFilterToggle, statusCheckboxGrid} from "../../../../library/data/GeoWorks/GeoWorks.data";
import {GeoWorksMarkerDict} from "../../../../type/data/module/GeoWorks/marker.type";
import {LatLngExpression} from "leaflet";
import {useAppDispatch, useAppSelector} from "../../../../store/hooks";
import {useMarkerFilter} from "../hooks/useMarkerFilter";

export const GeoWorks: React.FC = () => {

    const baseMarkerArray = useAppSelector((state) => {
        return state.marker.value
    })

    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////

    const [masterFilterToggleSelection, setMasterFilterToggleSelection] = useState<CheckboxOptions[][]>(masterFilterToggle)
    const [jobCheckboxGridSelection, setJobCheckboxGridSelection] = useState<CheckboxOptions[][]>(jobCheckboxGrid)
    const [statusCheckboxGridSelection, setStatusCheckboxGridSelection] = useState<CheckboxOptions[][]>(statusCheckboxGrid)
    const [showJobAddNewModal, setShowJobAddNewModal] = useState(false)
    const [selectedMarkerDict, setSelectedMarkerDict] = useState<GeoWorksMarkerDict | null>(null)
    const [addNewMarkerPosition, setAddNewMarkerPosition] = useState<LatLngExpression>([0,0])
    const [searchString, setSearchString] = useState<string>('')

    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////

    const markerArray = useMarkerFilter(baseMarkerArray, masterFilterToggleSelection, jobCheckboxGridSelection, statusCheckboxGridSelection, searchString)

    useEffect(() => {
        setJobCheckboxGridSelection((checkboxGrid: CheckboxOptions[][]) => {
            checkboxGrid.forEach((checkboxRow) => {
                checkboxRow.forEach((checkboxItem) => {
                    checkboxItem.disabled = !masterFilterToggle[0][0].checked
                })
            })
            return [...checkboxGrid]
        })
        setStatusCheckboxGridSelection((checkboxGrid: CheckboxOptions[][]) => {
            checkboxGrid.forEach((checkboxRow) => {
                checkboxRow.forEach((checkboxItem) => {
                    checkboxItem.disabled = !masterFilterToggle[0][1].checked
                })
            })
            return [...checkboxGrid]
        })
    }, [masterFilterToggleSelection])

    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////

    const dispatch = useAppDispatch()

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    const handleSaveMarkerDetailModal = (mapMarker: GeoWorksMarkerDict) => {
        dispatch(updateMarker(mapMarker))
        setSelectedMarkerDict(null)
    }

    const handleDeleteMarkerDetailModal = (id: string) => {
        dispatch(deleteMarker(id))
        setSelectedMarkerDict(null)
    }

    const handleCancelMarkerDetailModal = () => {
        setSelectedMarkerDict(null)
    }

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    const handleSaveNewMarker = (label: string, position: LatLngExpression) => {
        dispatch(createMarker({
            id: v4(),
            label: label,
            position: position,
            note: '',
            contact: {
                type: "resident",
                alias: 'Bonny'
            },
            jobs: [],
            access: 'private'
        }))
        setShowJobAddNewModal(false)
    }

    const handleCancelNewMarker = () => {
        setShowJobAddNewModal(false)
    }

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    const handleSelectMarker = (data: GeoWorksMarkerDict) => {
        setSelectedMarkerDict(data)
    }

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    return (
        <>
            <MainWrapper
                className={'MapWrapper'}
            >
                <MarkerPaneWrapper
                    className={'MapMarkerPaneWrapper'}
                >
                    <MarkerPane
                        onSelectMarker={handleSelectMarker}
                        masterFilterToggleSelection={masterFilterToggleSelection}
                        setMasterFilterToggleSelection={setMasterFilterToggleSelection}
                        jobCheckboxGridSelection={jobCheckboxGridSelection}
                        setJobCheckboxGridSelection={setJobCheckboxGridSelection}
                        statusCheckboxGridSelection={statusCheckboxGridSelection}
                        searchString={searchString}
                        setSearchString={setSearchString}
                        setStatusCheckboxGridSelection={setStatusCheckboxGridSelection}
                        markerArray={markerArray}
                    />
                </MarkerPaneWrapper>
                <MapPaneWrapper>
                    <MapPane
                        setShowJobAddNewModal={setShowJobAddNewModal}
                        setAddNewMarkerPosition={setAddNewMarkerPosition}
                        showJobAddNewModal={showJobAddNewModal}
                        markerArray={markerArray}
                        onSelectMarker={handleSelectMarker}
                    />
                </MapPaneWrapper>
            </MainWrapper>
            <JobDetailModalWrapper>
                {
                    selectedMarkerDict && (
                        <MarkerDetailModal
                            onSave={handleSaveMarkerDetailModal}
                            onCancel={handleCancelMarkerDetailModal}
                            mapMarker={selectedMarkerDict}
                            onDelete={handleDeleteMarkerDetailModal}
                        />
                    )
                }
            </JobDetailModalWrapper>
            <JobAddNewModalWrapper>
                <MarkerAddNewDialog
                    newMarkerPosition={addNewMarkerPosition}
                    onSave={handleSaveNewMarker}
                    onCancel={handleCancelNewMarker}
                    visible={showJobAddNewModal}
                />


            </JobAddNewModalWrapper>
        </>
    )
}