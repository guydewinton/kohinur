import {getModelTableName} from "../../../../../action/internal/TypeOrm/model/helpers";

export const polyInsertDeleteHandler = (constructor: Function) => {
    return async () => {
        const polyTableName = await getModelTableName(constructor)
            return `

CREATE OR REPLACE FUNCTION poly_insert_delete_handler() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            DELETE FROM ${polyTableName} WHERE ${polyTableName}.id = OLD.id;
            RETURN NULL;
        ELSIF (TG_OP = 'INSERT') THEN
              INSERT INTO ${polyTableName} (id, name) VALUES (NEW.id, TG_TABLE_NAME);
            RETURN NULL;
        END IF;
    END;
$$ LANGUAGE plpgsql;

`
    }

}


// export const nodeUpdateHandler = `
//
// CREATE OR REPLACE FUNCTION node_insert_delete_handler() RETURNS TRIGGER AS $$
//     DECLARE
//       node_id varchar;
//     BEGIN
//         IF (TG_OP = 'DELETE') THEN
//             DELETE FROM node_model WHERE node_model.id = OLD.id;
//             RETURN NULL;
//         ELSIF (TG_OP = 'INSERT') THEN
//               INSERT INTO node_model (name) VALUES (TG_TABLE_NAME) RETURNING id INTO node_id;
//                 NEW.id := node_id;
//             RETURN NEW;
//         END IF;
//     END;
// $$ LANGUAGE plpgsql;
//
// `

