import Document, { Html, Head, Main, NextScript } from 'next/document'
import styled from "styled-components";

const MainWrapper = styled(Main)`
    height: 100%;
    width: 100%;
`

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument