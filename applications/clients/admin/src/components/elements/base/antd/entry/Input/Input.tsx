import styled from "styled-components";

import {Input} from "antd/es";
import "antd/es/input/style"

const StyledInput = styled(Input)`
`

export const InputWidget = StyledInput;

export type { InputProps, InputRef, GroupProps, SearchProps, TextAreaProps, PasswordProps } from 'antd/es/input';
