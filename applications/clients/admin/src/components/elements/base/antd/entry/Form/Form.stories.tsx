import { FormWidget } from "./Form";
import React, {ChangeEvent, useState} from "react";
import {InputWidget} from "../Input/Input";
import {
    StyledButtonSvgWrapper,
    StyledIconButton
} from "../../../../../templates/GeoWorks/Modals/MarkerDetailModal/MarkerDetailModal.style";
import {UserCircleIcon} from "../../../../icons/svg/UserCircle/UserCircle";

export default {
  title: "Base / Antd / Form",
};
export const CheckboxForm = () => {

  const [value, setValue] = useState<string>('')

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
      setValue(event.target.value)
  }

  return (
      <FormWidget labelCol={{span: 1}} wrapperCol={{span: 5, offset: 0}}>

          <FormWidget.Item label={'Label'}>
              <InputWidget
                  value={value}
                  onChange={handleChange}
                  onKeyPress={(event) => {
                      if (event.key === "Enter") {
                          alert('Enter!')
                      }
                  }}
              />
          </FormWidget.Item>

          <FormWidget.Item label={'Contact'}>
              <StyledIconButton >
                  {'Bob Harris'}
                  <StyledButtonSvgWrapper>
                      <UserCircleIcon />
                  </StyledButtonSvgWrapper>
              </StyledIconButton>
          </FormWidget.Item>

      </FormWidget>
  )
};
CheckboxForm.storyName = "Basic Form";
