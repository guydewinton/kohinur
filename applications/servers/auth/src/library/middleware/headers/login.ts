import {NextFunction, Request, Response} from "express";


export const transposeRefererHeader = (req: Request, res: Response, next: NextFunction) => {

    res.header('x-referer', req.header('referer:'))

    next()

}

export default transposeRefererHeader;