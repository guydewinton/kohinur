import styled from "styled-components";
import Title from "antd/es/typography/Title";

const StyledTitle = styled(Title)``

export const TitleWidget = StyledTitle

export type {TitleProps} from 'antd/es/typography/Title'