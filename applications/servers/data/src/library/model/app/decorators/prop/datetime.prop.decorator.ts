import {TypeOrmPropBaseOptions, TypeOrmPropExtendedOptions} from "../../../../../type/options.type";
import {GraphqlField} from "../../../api/gql/decorators/prop/gql.prop.decorator";
import {Column, CreateDateColumn, UpdateDateColumn} from "typeorm";

export const CreatedDateTimeField = (options?: TypeOrmPropBaseOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Date, options)
        CreateDateColumn(options)(prototype, propertyName)
    }
}

export const ModifiedDateTimeField = (options?: TypeOrmPropBaseOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Date, options)
        UpdateDateColumn(options)(prototype, propertyName)
    }
}

export const DateTimeField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Date, options)
        Column('timestamp', options)(prototype, propertyName)
    }
}

export const DateField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Date, options)
        Column('date', options)(prototype, propertyName)
    }
}

export const TimeField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Date, options)
        Column('time', options)(prototype, propertyName)
    }
}
