import {TypeOrmPropExtendedOptions, TypeOrmVarCharPropOptions} from "../../../../../type/options.type";
import {GraphqlField} from "../../../api/gql/decorators/prop/gql.prop.decorator";
import {Column} from "typeorm";

export const VarCharField = (options?: TypeOrmVarCharPropOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, String, options)
        Column('varchar', options)(prototype, propertyName)
    }
}

export const TextField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, String, options)
        Column('text', options)(prototype, propertyName)
    }
}