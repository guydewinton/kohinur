import styled from "styled-components";
import Sider from "antd/es/layout/Sider";

const StyledSider = styled(Sider)``

export const SiderWidget = StyledSider

export type {SiderProps} from 'antd/es/layout'