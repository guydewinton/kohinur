import {HousePinIcon} from "../../../icons/svg/HousePin/HousePin";
import React from "react";
import {StyledHouse, StyledIconWrapper} from "./HousePin.style";

export const HousePin = () => {
    return (
        <StyledIconWrapper>
            <StyledHouse>
                <HousePinIcon/>
            </StyledHouse>
        </StyledIconWrapper>
    )
}