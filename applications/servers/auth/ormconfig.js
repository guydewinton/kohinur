require('dotenv').config()
const fs = require('fs')

module.exports = {
   "type": "postgres",
   "host": process.env.POSTGRES_HOST,
   "port": process.env.POSTGRES_PORT,
   "username": process.env.POSTGRES_USER,
   "password": fs.readFileSync(process.env.POSTGRES_PASSWORD_FILE).toString(),
   "database": process.env.POSTGRES_DB,

   "synchronize": true,
   "logging": false,
   "entities": [
      "src/schema/**/*.ts"
   ],
   "migrations": [
      "db_typeorm/migration/**/*.ts"
   ],
   "subscribers": [
      "db_typeorm/subscriber/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "src/schema",
      "migrationsDir": "db_typeorm/migration",
      "subscribersDir": "db_typeorm/subscriber"
   }
}