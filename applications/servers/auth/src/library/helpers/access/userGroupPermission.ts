const userGroupPermission = (routeAccessGroups: string[], userAccessGroups: string[]) => {

    let hasPermission = false
    userAccessGroups.forEach((userAccessGroup) => {
        if (routeAccessGroups.includes(userAccessGroup)) {
            hasPermission = true
        }
    })
    return hasPermission
}

export default userGroupPermission