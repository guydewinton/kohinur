import {useEffect, useState} from "react";

const useAppMounted = () => {
    const [hasMounted, setHasMounted] = useState(false);

    useEffect(() => {
        if (typeof window !== 'undefined') {
            setHasMounted(true);
        }
    }, []);

    return hasMounted;
}

export default useAppMounted