import "reflect-metadata";
import dotenv from "dotenv";
dotenv.config();

import express from 'express';
import rootRouter from "./routes/http/root.http.route";
import {createConnection} from "typeorm";
import {rootSqlQuery} from "./queries/sql/root.sql";
import {WebSocketServer} from "ws";
import {useServer} from "graphql-ws/lib/use/ws";
import {graphqlHTTP} from "express-graphql";
import GraphqlPlayground from 'graphql-playground-middleware-express';
import {buildSchema} from "type-graphql";
import HelloResolver from "./routes/gql/hello.resolver.gql";
import processHasuraTableWatchList from "./library/action/external/hasura/class/table.class.hasura";
import {connectHasuraDB} from "./library/action/external/hasura/api/connectDB.hasura";
import {processHasuraRelationsList} from "./library/action/external/hasura/prop/relations.prop.hasura";


(async () => {

    try {
        await createConnection();
    } catch (err) {
        console.log(err)
    }

    await rootSqlQuery();
    await connectHasuraDB();
    await processHasuraTableWatchList();
    await processHasuraRelationsList()

    const server = new WebSocketServer({
        port: process.env.WS_SERVER_PORT,
        path: '/graphql',
    });

    const schema = await buildSchema({
        resolvers: [HelloResolver]
    });

    useServer({ schema }, server);

    const app = express();

    app.use('/api/v1/graphql', graphqlHTTP({ schema }));
    app.get('/api/v1/playground', GraphqlPlayground({ endpoint: '/api/v1/graphql' }))

    app.use('/api/v1', rootRouter)

    app.listen(process.env.HTTP_SERVER_PORT, () => {
        console.log(`HTTP Server started... listening on port ${process.env.HTTP_SERVER_PORT}.`);
    });

    console.log(`WS Server started on port ${process.env.WS_SERVER_PORT}`)

})()




// import ws from 'ws';
// import express from 'express';
// import { graphql } from 'graphql';
// import { graphqlHTTP } from 'express-graphql';
// import GraphqlPlayground from 'graphql-playground-middleware-express';
//
// import rootRouter from "./routes/http/certs.http.route";

// import {buildSchema} from "type-graphql";
// import LiveDirective from "./library/graphql/directives/LiveDirective";
// import { useServer } from 'graphql-ws/lib/use/ws';

// import WebSocketServer from 'ws';

// const server = new WebSocketServer.Server({
//     port: 4000,
//     path: '/api/v1/graphql',
// });

//
// const schema = await buildSchema({
//     resolvers: [UserResolver],
//     directives: [LiveDirective]
// });
//
//
// useServer(
//     { schema },
//     server,
// );


// (async () => {
//
//     const schema = await buildSchema({
//         resolvers: [UserResolver],
//         directives: [LiveDirective]
//     });
//
//     const app = express();
//     app.use('/api/v1', rootRouter)
//     app.use('/api/v1/graphql', graphqlHTTP({ schema }));
//     app.get('/api/v1/playground', GraphqlPlayground({ endpoint: '/api/v1/graphql' }))
//
//     // const wsGraphqlServer = makeServer({schema})
//
//     const wsServer = new ws.Server({
//         port: 5000,
//         path: '/api/v1/graphql',
//     });
//
//     wsServer.on('connection', (socket, _request) => {
//         socket.send(JSON.stringify({a: true}))
//         socket.on('message', (message) => {
//             console.log(typeof message.toString())
//             console.log('message:', message.toString())
//             socket.send('message', () => {
//                 return JSON.stringify({a: true})
//             })
//             graphql(schema, message.toString()).then((result) => {
//
//                 console.log(result.data!);
//                 socket.send(JSON.stringify(result));
//             });
//         })
//     })
//
//
//
//     app.listen(4000, () => {
//         // const wsServer = new ws.Server({
//         //     server,
//         //     path: '/api/v1/graphql',
//         // });
//
//         // useServer({
//         //     schema,
//         //     context: (ctx, msg, args) => {
//         //         return [ctx, msg, args];
//         //     },
//         //     // todo: two ws connections... one for graphql queries and one for live updates / subscriptions (maybe three?)
//         //     // todo: can it be done with one and some kind of routing???
//         // },
//         //     wsServer
//         // );
//
//         console.log('Server started... listening on port 4000.');
//     });
// })();




