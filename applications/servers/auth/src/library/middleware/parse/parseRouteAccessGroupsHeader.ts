import {NextFunction, Request, Response} from "express";

const parseRouteAccessGroupsHeader = (req: Request, res: Response, next: NextFunction) => {

    const accessGroupsHeader = req.header('x-access-groups')
    if (!accessGroupsHeader) {
        console.log('Access Groups Header Undefined')
        return res.status(401).send('Access Groups Header Undefined');
    }
    let routeAccessGroups;
    try {
        routeAccessGroups = JSON.parse(accessGroupsHeader);
    } catch (err) {
        console.log('Invalid Access Groups Header JSON Encoding')
        return res.status(401).send('Invalid Access Groups Header JSON Encoding');
    }
    if (!Array.isArray(routeAccessGroups) || !routeAccessGroups.every(accessRoute => typeof accessRoute === 'string')) {
        console.log('Invalid Access Groups Header')
        return res.status(401).send('Invalid Access Groups Header');
    }
    res.locals.routeAccessGroups = routeAccessGroups as string[];
    return next()

}

export default parseRouteAccessGroupsHeader;