import React, {Dispatch, SetStateAction, useCallback, useContext, useMemo} from "react";
import ReactDOMServer from 'react-dom/server';
import {LatLngExpression, DivIcon} from "leaflet";
import {
    MapCustomMarker,
} from "../../../../elements/widgets/map/MapCustomMarker/MapCustomMarker.index";
import {GeoWorksMarkerDict} from "../../../../../type/data/module/GeoWorks/marker.type";
import {MapWrapper, StyledMapPopupLink} from "./MapPane.style";
import {HousePin} from "../../../../elements/widgets/pins/HousePin/HousePin.index";
import {CheckboxOptions} from "../../../../elements/widgets/checkboxes/Checkbox.type";

export interface MapPaneProps {
    markerArray: GeoWorksMarkerDict[]
    showJobAddNewModal: boolean
    setShowJobAddNewModal: Dispatch<SetStateAction<boolean>>
    setAddNewMarkerPosition: Dispatch<SetStateAction<LatLngExpression>>
    onSelectMarker: (data: GeoWorksMarkerDict) => void
}

const center: LatLngExpression  = [-28.4924, 153.4003]

export const MapPane: React.FC<MapPaneProps> = ({
                                      setShowJobAddNewModal,
                                      setAddNewMarkerPosition,
                                      showJobAddNewModal,
                                      markerArray,
                                      onSelectMarker
                                  }) => {


    const mapMarkerCallback = (_mapMarker: GeoWorksMarkerDict): DivIcon => {
        return new DivIcon({html: ReactDOMServer.renderToString(<HousePin/>)})
    }

    const handleAddNewMarker = (markerPosition: LatLngExpression) => {
        setAddNewMarkerPosition(markerPosition)
        setShowJobAddNewModal(true)
    }

    const MapMarkerComponent: React.FC<{mapMarker: GeoWorksMarkerDict}> = useCallback(({
        mapMarker
            }) => {

        const handleClickPopupLink = () => {
            onSelectMarker(mapMarker)
        }

        return (
            <>
                <p>{mapMarker.label}</p>
                <StyledMapPopupLink onClick={handleClickPopupLink}>See Details</StyledMapPopupLink>
            </>

        )
    }, [])

    return (
        <MapWrapper>
            <MapCustomMarker
                markerArray={markerArray}
                onAddNewMarker={handleAddNewMarker}
                addingNewMarker={showJobAddNewModal}
                center={center}
                zoom={16}
                markerIconCallback={mapMarkerCallback}
                MarkerPopupContent={MapMarkerComponent}
            />
        </MapWrapper>
    )

}