import {Contact} from "../contact.model";
import {ContactPointType} from "../../../../type/schema.type";
import {SecondaryBaseModel} from "../../../abstract/secondary.base.model";
import {VarCharField} from "../../../../library/model/app/decorators/prop/character.prop.decorator";
import {ManyToOneRelation} from "../../../../library/model/app/decorators/prop/relation.prop.decorator";
import SecondaryModel from "../../../../library/model/app/decorators/class/SecondaryModel.class.decorator";

@SecondaryModel()
export class ContactPoint extends SecondaryBaseModel {

    @VarCharField()
    name: string

    @VarCharField()
    type: ContactPointType

    @VarCharField()
    value: string

    @ManyToOneRelation(() => Contact, contact => contact.contactPoints)
    contact: Contact

}