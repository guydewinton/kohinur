import jwt from "../jwt";

export const validateAccessToken = (accessToken: string) => jwt.validateAccessToken(accessToken);

export const validateRefreshToken = (accessToken: string) => jwt.validateRefreshToken(accessToken);