import {VarCharField} from "../../library/model/app/decorators/prop/character.prop.decorator";
import {PrimaryUUIDField} from "../../library/model/app/decorators/prop/identifier.prop.decorator";

export class PolyBaseModel {

    @PrimaryUUIDField()
    id: string;

    @VarCharField()
    name: string;

}
