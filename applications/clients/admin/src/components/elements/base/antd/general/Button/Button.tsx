import styled from "styled-components";
import {Button} from 'antd/es';
import 'antd/es/button/style'

const StyledButton = styled(Button)`
`

export const ButtonWidget = StyledButton

export type { ButtonProps, ButtonShape, ButtonType, ButtonGroupProps, ButtonSize } from 'antd/es/button';
