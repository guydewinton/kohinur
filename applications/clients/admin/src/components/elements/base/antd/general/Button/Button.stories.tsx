import { ButtonWidget } from "./Button";

export default {
  title: "Base / Antd / Button",
};
export const Button = () => <ButtonWidget>My Button</ButtonWidget>;
Button.storyName = "Basic Button";
export const ButtonRounded = () => <ButtonWidget shape={'round'}>My Button</ButtonWidget>;
ButtonRounded.storyName = "Rounded Button";