const { app, BrowserWindow, ipcMain } = require('electron');
const nodePath = require('path');

const createWindow = async () => {

    const win = new BrowserWindow({
        width: 800,
        height: 600,
        autoHideMenuBar: true,
        icon: 'electron/config/512x512.png',
        frame: false,
        webPreferences: {
            nodeIntegration: false,
            preload: nodePath.join(__dirname, 'preload.electron.js')
        }
    });

    await win.loadFile('build/index.html');

    return win;

};

app.whenReady().then(() => {
    createWindow()
        .then(win => {
            win.maximize()
            ipcMain.on('window:minimize', () => {
                win.minimize();
            })

            ipcMain.on('window:maximize', () => {
                win.maximize();
            })

            ipcMain.on('window:toggle', () => {
                win.isMaximized() ? win.restore() : win.maximize();
            })
            ipcMain.on('window:restore', () => {
                win.restore();
            })
            ipcMain.on('window:close', () => {
                win.close();
            })
        });


});


