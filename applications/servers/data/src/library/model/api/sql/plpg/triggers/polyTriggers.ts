import {getModelTableName} from "../../../../../action/internal/TypeOrm/model/helpers";

export const polyInsertTrigger = (model: Function) => {
    return async () => {
        const tableName = await getModelTableName(model())
        const triggerName = `${tableName}_poly_insert`
        return `
        DROP TRIGGER IF EXISTS ${triggerName} on ${tableName};   
        CREATE TRIGGER ${triggerName}
            AFTER INSERT ON ${tableName}
                FOR EACH ROW
                    EXECUTE FUNCTION poly_insert_delete_handler();
    `
    }
}

export const polyDeleteTrigger = (model: Function) => {
    return async () => {
        const tableName = await getModelTableName(model())
        const triggerName = `${tableName}_poly_delete`
        return `
        DROP TRIGGER IF EXISTS ${triggerName} on ${tableName};   
        CREATE TRIGGER ${triggerName}
            AFTER DELETE ON ${tableName}
                FOR EACH ROW
                    EXECUTE FUNCTION poly_insert_delete_handler();
    `
    }
}

// export const NodeUpdateTrigger = (name: string) => {
//     return `
//         DROP TRIGGER IF EXISTS ${name}_delete on ${name};
//         CREATE TRIGGER ${name}_delete
//             AFTER DELETE ON ${name}
//                 FOR EACH ROW
//                     EXECUTE FUNCTION node_insert_delete_handler();
//     `
// }