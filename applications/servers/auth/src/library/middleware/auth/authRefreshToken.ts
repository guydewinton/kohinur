import {User} from "../../../schema/User.model";
import {Group} from "../../../schema/Group.model";
import userGroupPermission from "../../helpers/access/userGroupPermission";
import {NextFunction, Request, Response} from "express";
import {validateRefreshToken} from "../../helpers/crypto/token/jwt/defaults/jwtValidator";

const authRefreshToken = async (_req: Request, res: Response, next: NextFunction) => {

    if (!validateRefreshToken(res.locals.refreshToken)) {
        console.log('Invalid Refresh Token')
        return res.status(401).send('Invalid Refresh Token');
    }

    let user;
    try {
        user = await User.findOne(res.locals.refreshTokenBody.userId);
    } catch (err) {
        console.log('DB Error')
        return res.status(400).send('DB Error');
    }

    if (!user) {
        console.log('Invalid User')
        return res.status(401).send('Invalid User');
    }

    if (user.status === 'INACTIVE' || user.status === 'PENDING') {
        console.log(`User status is ${user.status}`)
        return res.status(401).send(`User status is ${user.status}`);
    }

    const userGroupList = user.groups.map((group: Group) => group.group)

    if (!userGroupPermission(userGroupList, res.locals.routeAccessGroups)) {
        console.log('Invalid User Group Permissions')
        return res.status(401).send('Invalid User Group Permissions')
    }

    res.locals.user = user
    res.locals.userGroupList = userGroupList
    res.locals.status = 201

    return next()

};

export default authRefreshToken;
