import {Connection} from "typeorm";
import {refInsertDeleteHandler} from "../../library/model/api/sql/plpg/functions/refHandlers";
import {sqlTriggerList} from "../../library/model/api/sql/plpg/triggers/triggerList";
import {DBConnection} from "../../library/action/internal/TypeOrm/database/getConnection";

export const rootSqlQuery = async () => {

    const entityManager: Connection = await DBConnection();

    try {
        await entityManager.query(refInsertDeleteHandler);
        for (let i: number = 0; i < sqlTriggerList.length; i++) {
            const query = await sqlTriggerList[i]();
            await entityManager.query(query);
        }

    } catch (err) {
        console.log(err)
        throw err;
    }

}
