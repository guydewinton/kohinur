import {Request, Response, Router} from "express";


const secretRouter = Router();

secretRouter.get('admin/secret/rotate_access', async (_req: Request, res: Response) => {
    return res.send()
})

secretRouter.get('admin/secret/rotate_refresh', async (_req: Request, res: Response) => {
    return res.send()
})

secretRouter.get('admin/secret/rotate_service', async (_req: Request, res: Response) => {
    return res.send()
})

export default secretRouter;