import {Router} from "express";
import {validate} from "class-validator";

import {User} from "../../schema/User.model";
import {Group} from "../../schema/Group.model";
import {createHash} from "../../library/helpers/crypto/hashing/hash";


const userRouter = Router();

userRouter.get('/get-users', async (req, res) => {
    const take = req.query.take ? +req.query.take : 10
    const page = req.query.page ? +req.query.page : 1;
    const skip = (page-1) * take ;
    const users = await User.findAndCount({
        take: take,
        skip: skip
    });
    return res.send(users)
    // todo: remove this endpoint for production and create pagination middleware
});

userRouter.post('/create-user', async (req, res) => {

    if (!req.body.email || typeof req.body.email !== 'string') {
        return res.status(400).send('Invalid Email')
    }

    if (!req.body.password || typeof req.body.password !== 'string') {
        return res.status(400).send('Invalid Password')
    }

    if (!req.body.status || typeof req.body.status !== 'string') {
        return res.status(400).send('Invalid Status')
    }

    if (!req.body.groups || !Array.isArray(req.body.groups) || !req.body.groups.every((group: any) => typeof group === 'string')) {
        return res.status(400).send('Invalid Groups')
    }

    const user = new User();

    user.email = req.body.email;
    user.status = req.body.status;
    try {
        user.password = await createHash(req.body.password);
    } catch (err) {
        return res.status(400).send()
    }

    console.log(req.body.groups, typeof req.body.groups)
    user.groups = req.body.groups.map((group: Group) => {
        return {
            group,
            date: new Date()
        }
    })

    const errors = await validate(user)

    if (errors.length > 0) {
        return res.status(400).send()
    }

    try {
        await user.save()
    } catch (err) {
        console.log(err)
        return res.status(400).send(err)
    }

    return res.status(200).send({
        id: user.id,
        email: user.email,
        status: user.status,
        groups: user.groups
    })
});

userRouter.patch('/update-user', async (req, res) => {
    const user: User = await User.findOne(req.body.id)
    user.email = req.body.email
    const errors = await validate(user)
    if (errors.length > 0) {
        return res.status(400).send()
    }
    try {
        await user.save()
    } catch (err) {
        console.log(err)
        return res.status(400).send(err)
    }
    return res.status(200).send({
        id: user.id,
        email: user.email
    })
});

userRouter.delete('/delete-user', async (req, res) => {
    try {
        await User.delete(req.body.id)
    } catch (err) {
        console.log(err)
        return res.status(400).send(err)
    }
    return res.send()
});

userRouter.get('/send-reset-password-link', (_req, res) => {
    return res.send('hello')
});

userRouter.get('/reset-password', (_req, res) => {
    return res.send('hello')
});

export default userRouter;


