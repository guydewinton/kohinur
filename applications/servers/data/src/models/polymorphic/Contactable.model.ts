import {PolyBaseModel} from "../abstract/poly.base.model";
import {Person} from "../primary/person/person.model";
import {Company} from "../primary/company/company.model";
import PolyModel from "../../library/model/app/decorators/class/PolyModel.class.decorator";
import {OneToManyRelation} from "../../library/model/app/decorators/prop/relation.prop.decorator";
import {ContactSet} from "../primary/contact/contact.set.model";

@PolyModel([() => Person, () => Company])
export class Contactable extends PolyBaseModel {

    @OneToManyRelation(() => ContactSet, (contactSet) => contactSet.contactable)
    contactSet: ContactSet[]
}

// circular import problem...
