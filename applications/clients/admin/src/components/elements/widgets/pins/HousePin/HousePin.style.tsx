import styled from "styled-components";

export const StyledIconWrapper = styled.div`
display: flex;
  justify-content: center;
  align-items: center;
  
`

export const StyledHouse = styled.div`
  height: 50px;
  width: 50px;
  position: relative;
  bottom: 25px;
  z-index: 100;
  svg {
    height: 40px;
    width: 40px;
  }
`