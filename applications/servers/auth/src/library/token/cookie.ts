import {CookieOptions} from "express";

const oneWeek = 7 * 24 * 3600 * 1000

export const cookieOptions: CookieOptions = {
        httpOnly: true,
        domain:'.mullumfoodcoop.com',
        sameSite: 'strict',
        secure: true,
        expires: new Date(Date.now() + oneWeek),
        maxAge: oneWeek
    }