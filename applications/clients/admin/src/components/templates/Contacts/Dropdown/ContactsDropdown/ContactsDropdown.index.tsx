import React, {useLayoutEffect, useRef, useState} from "react";
import { InputWidget } from "../../../../elements/base/antd/entry/Input/Input";
import {
    ContactFilterItemWrapper, ContactsDropdownInnerWrapper,
    ContactsDropdownOuterWrapper, ContactsFilterWrapper,
    ContactsSidebarWrapper, ContactsTableInnerWrapper,
    ContactsTableOuterWrapper, FilterHeaderText,
    FilterHeaderWrapper
} from "./ContactsDropdown.style";
import {CheckboxGridWidget} from "../../../../elements/widgets/checkboxes/CheckboxGrid/CheckboxGrid";
import { CheckboxWidget } from "../../../../elements/base/antd/entry/Checkbox/Checkbox";
import { TableWidget } from "../../../../elements/base/antd/display/Table/Table";
import {jobCheckboxGrid} from "../../../../../library/data/GeoWorks/GeoWorks.data";

const dataSource = [
    {
        key: '1',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '2',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '3',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '4',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '5',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '6',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '7',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '8',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '9',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '2',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '10',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '11',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '12',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '2',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '13',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '14',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '1',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '2',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '3',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '4',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '5',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '6',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '7',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '8',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '9',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '2',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '10',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '11',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '12',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '2',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
    {
        key: '13',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
    },
    {
        key: '14',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
    },
];

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
    },
];

export const ContactsDropdown: React.FC = () => {

    const [tableHeight, setTableHeight] = useState(0)
    const tableWrapperRef = useRef<HTMLDivElement>(null)

    useLayoutEffect(() => {
        if (tableWrapperRef.current) {
            setTableHeight(tableWrapperRef.current.clientHeight - 55)
        }
    }, [tableWrapperRef?.current?.clientHeight])

    return (
        <ContactsDropdownOuterWrapper>
            <ContactsDropdownInnerWrapper>
                <ContactsSidebarWrapper>
                    <InputWidget.Search/>
                    <ContactsFilterWrapper>
                        <FilterHeaderWrapper>
                            <FilterHeaderText>Skills</FilterHeaderText>
                            <CheckboxWidget />
                        </FilterHeaderWrapper>
                        <ContactFilterItemWrapper>
                            <CheckboxGridWidget
                                checkboxArray={jobCheckboxGrid}
                                span={12}
                                onChange={() => {}}
                            />
                        </ContactFilterItemWrapper>

                    </ContactsFilterWrapper>
                </ContactsSidebarWrapper>
                <ContactsTableOuterWrapper>
                    <ContactsTableInnerWrapper ref={tableWrapperRef}>
                        <TableWidget scroll={{y: tableHeight}} dataSource={dataSource} columns={columns} pagination={false}/>

                    </ContactsTableInnerWrapper>
                </ContactsTableOuterWrapper>
            </ContactsDropdownInnerWrapper>

        </ContactsDropdownOuterWrapper>
    )

}

