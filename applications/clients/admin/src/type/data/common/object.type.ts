export interface GenericObject {
    [key: string | symbol]: any
}

export interface DataObject extends GenericObject{
    id: string | number
}

export type ExtendedGenericObject <T> = T & GenericObject