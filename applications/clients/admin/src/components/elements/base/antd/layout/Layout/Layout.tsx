import styled from "styled-components";
import Layout from "antd/es/layout/layout";
import 'antd/es/layout/style'

const StyledLayout = styled(Layout)``

export const LayoutWidget = StyledLayout

export type {LayoutProps} from 'antd/es/layout'