import {ID} from "type-graphql";
import {Column, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";
import {TypeOrmPropBaseOptions, TypeOrmPropExtendedOptions} from "../../../../../type/options.type";
import {GraphqlField} from "../../../api/gql/decorators/prop/gql.prop.decorator";

export const PrimaryGeneratedUUIDField = (options?: TypeOrmPropBaseOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, ID, options)
        PrimaryGeneratedColumn('uuid', options)(prototype, propertyName)
    }
}

export const PrimaryUUIDField = (options?: TypeOrmPropBaseOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, ID, options)
        PrimaryColumn('uuid', options)(prototype, propertyName)
    }
}

export const UUIDField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, ID, options)
        Column('uuid', options)(prototype, propertyName)
    }
}
