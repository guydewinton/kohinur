import {Float, Int} from "type-graphql";
import {Column} from "typeorm";
import {
    TypeOrmFloatPropOptions,
    TypeOrmPropExtendedOptions
} from "../../../../../type/options.type";
import {GraphqlField} from "../../../api/gql/decorators/prop/gql.prop.decorator";

export const SmallIntField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Int, options)
        Column('smallint', options)(prototype, propertyName)
    }
}

export const IntegerField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Int, options)
        Column('integer', options)(prototype, propertyName)
    }
}

export const BigIntField = (options?: TypeOrmPropExtendedOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Int, options)
        Column('bigint', options)(prototype, propertyName)
    }
}

export const RealFloatField = (options?: TypeOrmFloatPropOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Float, options)
        Column('real', options)(prototype, propertyName)
    }
}

export const DoubleFloatField = (options?: TypeOrmFloatPropOptions) => {
    return (prototype: Object, propertyName: string) => {
        GraphqlField(prototype, propertyName, Float, options)
        Column('double', options)(prototype, propertyName)
    }
}
