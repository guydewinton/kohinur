import crypto from "crypto";
import {sign, verify} from "jsonwebtoken";

class JwtHandler {

    private accessSecret: string;
    private refreshSecret: string;
    private serviceSecret: string;

    constructor() {
        this.accessSecret = process.env.ACCESS_TOKEN_SECRET!;
        this.refreshSecret = process.env.ACCESS_TOKEN_SECRET!;
        this.serviceSecret = process.env.ACCESS_TOKEN_SECRET!;

    }

    generateAccessToken = (userId: string, status: string, groups: string[]) => {
        return sign({userId, status, groups}, this.accessSecret, {expiresIn: process.env.ACCESS_TOKEN_EXPIRY})
    }

    generateRefreshToken = (userId: string) => {
        return sign({userId}, this.refreshSecret, {expiresIn: process.env.AUTH_TOKEN_EXPIRY})
    }

    generateServiceToken = (redirect: string) => {
        return sign({redirect}, this.serviceSecret, {expiresIn: process.env.APP_TOKEN_EXPIRY})
    }

    validateAccessToken = (token: string) => {
        try {
            verify(token, this.accessSecret)
            return true
        } catch (err) {
            console.log('Invalid access token')
            return false
        }

    }

    validateRefreshToken = (token: string) => {
        try {
            verify(token, this.refreshSecret)
            return true
        } catch (err) {
            console.log('Invalid refresh token')
            return false
        }
    }

    validateServiceToken = (token: string) => {
        try {
            verify(token, this.serviceSecret)
            return true
        } catch (err) {
            console.log(err)
            return false
        }
    }

    generateNewAccessSecret = () => {
        this.accessSecret = crypto.randomBytes(60).toString('hex');
    }

    generateNewRefreshSecret = () => {
        this.refreshSecret = crypto.randomBytes(60).toString('hex');
    }

    generateNewServiceSecret = () => {
        this.serviceSecret = crypto.randomBytes(60).toString('hex');
    }

}

export default JwtHandler;