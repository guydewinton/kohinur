import styled from "styled-components";
import {ButtonWidget} from "../../../../elements/base/antd/general/Button/Button";

export const StyledModalInnerWrapper = styled.div<{height: number}>`
  height: ${props => props.height ? (props.height - 180) * .9 : 0}px;
  width: 100%;
  display: flex;
  flex-direction: row;
`

export const StyledModalMapWrapper = styled.div`
  height: 100%;
  width: 30%;
  outline: 1px black solid;
`

export const StyledModalDataWrapper = styled.div`
  height: 100%;
  width: 70%;
  display: flex;
  flex-direction: column;
  outline: 1px black solid;

`

export const StyledModalMarkerDataWrapper = styled.div`
  height: 200px;
  width: 100%;
  display: flex;
`

export const StyledModalMarkerDataLeftWrapper = styled.div`
  width: 600px;
  padding: 10px 20px 0px 20px;
`

export const StyledModalMarkerDataLeftInnerWrapper = styled.div`

`

export const StyledDescriptionsTitle = styled.p`
  font-size: 18px;
  font-weight: bold;
  text-wrap: none;
  margin: 3px 0 15px 0;
`

export const StyledDescriptionsTitleNotes = styled.p`
  font-size: 18px;
  font-weight: bold;
  text-wrap: none;
  margin: 3px 0 5px 0;
`

export const StyledIconButton = styled(ButtonWidget)`
  display: flex;
  justify-content: space-between;
  width: 100%;
  text-align: left;
`

export const StyledButtonSvgWrapper =styled.div`
  height: 22px;
  width: 22px;
  padding: 1px;
`


export const StyledModalMarkerDataRightWrapper = styled.div`
  width: 100%;
  padding: 10px 0px 0px 20px;
  border-left: 1px black solid;
`

export const StyledTextAreaNotes = styled.textarea`
  height: calc(100% - 38px);
  width: 100%;
  resize: none;
  outline: none;
  border: none;
`

export const StyledModalJobDataWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  outline: 1px black solid;

`

export const StyledModalJobDataTableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: 1;
`

export const StyledModalJobDataTableInnerWrapper = styled.div`
  padding: 1px;
  flex: 1;
`

export const StyledJobToolBarWrapper =styled.div`
  display: flex;
  //height: 22px;
  width: 100%;
  padding: 0 1px;
`
