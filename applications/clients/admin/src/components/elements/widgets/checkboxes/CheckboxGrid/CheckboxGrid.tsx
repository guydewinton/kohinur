import React from "react";
import styled from "styled-components";
import {GridIndex} from "../../../../../type/data/common/array.type";
import {CheckboxOptions} from "../Checkbox.type";
import {CheckboxChangeEvent, CheckboxWidget} from "../../../base/antd/entry/Checkbox/Checkbox";
import { RowWidget } from "../../../base/antd/layout/Grid/Row";
import { ColWidget } from "../../../base/antd/layout/Grid/Col";

const CheckboxGridWrapper = styled.div`
  width: 100%;
`

const StyledCheckbox = styled(CheckboxWidget)`
  width: 100%;
`

interface CheckboxGroupProps {
    checkboxArray: CheckboxOptions[][]
    onChange: (gridIndex: GridIndex, event: CheckboxChangeEvent) => void
    span: number;
}

export const CheckboxGridWidget: React.FC<CheckboxGroupProps> = ({
            checkboxArray,
            onChange,
            span
        }) => {


    const CheckboxGrid = checkboxArray.map((checkboxRow, xIndex) => {
        return (
            <RowWidget
                key={xIndex}
            >
                {checkboxRow.map((checkboxItem, yIndex) => {
                    return (
                        <ColWidget
                            key={yIndex}
                            span={span}
                        >
                            <StyledCheckbox
                                onChange={(event) => onChange([xIndex, yIndex], event)}
                                disabled={checkboxItem.disabled ? checkboxItem.disabled : false}
                                checked={checkboxItem.checked}
                            >
                                {checkboxItem.label}
                            </StyledCheckbox>
                        </ColWidget>
                    )
                })}
            </RowWidget>
        )
    })

    return (
        <CheckboxGridWrapper>
            {CheckboxGrid}
        </CheckboxGridWrapper>
    )
}