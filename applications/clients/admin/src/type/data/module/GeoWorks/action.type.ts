import {ContactTradeDict} from "./contact.type";

export type JobTypeValueUnion = (
    'access' |
    'waterDamage' |
    'power' |
    'plumbing' |
    'mould' |
    'rubbish' |
    'gas' |
    'waterSupply' |
    'landSlippage' |
    'buildingIntegrity' |
    'councilBins' |
    'additional'
    )

export type JobTypeLabelUnion = (
    'Access' |
    'Water Damage' |
    'Power' |
    'Plumbing' |
    'Mould' |
    'Rubbish' |
    'Gas' |
    'Water Supply' |
    'Land Slippage' |
    'Building Integrity' |
    'Council Bins Lost' |
    'Additional'
    )

export interface JobsDetail {
    id: string
    issue: string;
    typeValue: JobTypeValueUnion | ''
    typeLabel: JobTypeLabelUnion | ''
    status: 'pending' | 'complete';
    assignee?: ContactTradeDict;
    works?: any[]
    startDate?: Date;
    completionDate?: Date;
}