import styled from "styled-components";
import XMarkIcon from "../../elements/icons/svg/XMark/XMark";
import AngleArrowIcon from "../../elements/icons/svg/AngleArrow/AngleArrow.element.index";
import AddressBookIcon from "../../elements/icons/svg/AddressBook/AddressBook";
import CommentIcon from "../../elements/icons/svg/Comment/Comment";
import CheckboxIcon from "../../elements/icons/svg/Checkbox/Checkbox";
import CalendarSolidIcon from "../../elements/icons/svg/CalendarSolid/CalendarSolid";
import {IconWrapper} from "../../elements/icons/containers/IconWrapper/IconWrapper";
import { DropdownWidget } from "../../elements/base/antd/navigation/Dropdown/Dropdown";
import React, {Dispatch, useState} from "react";
import {ContactsDropdown} from "../../templates/Contacts/Dropdown/ContactsDropdown/ContactsDropdown.index";

const HeaderWrapper = styled.div`
  height: 70px;
  width: 100%;
  display: flex;
  background-color: #213644; //213644
  border-bottom: #030d18 1px solid;


`

const MainBlock = styled.div`
  width: calc(100% - 35px);
  height: 100%;
  padding: 0 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  -webkit-user-select: none;
  -webkit-app-region: drag;
`

const AppBlock = styled.div`
  width: 35px;
  height: 100%;
  background-color: #091723;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;

  svg {
    fill: #e7e7e7;
  }

  padding: 3px 0;
`

const CloseIconWrapper = styled.div`
  width: 32px;
  height: 32px;
  display: flex;
  padding: 5px;
`

const MenuIconWrapper = styled.div`
  width: 32px;
  height: 32px;
  display: flex;
  padding: 3px;
  svg {
    stroke: #444444;
    stroke-width: 16px;
    
  }
`

const HeaderItemBlockWrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-top: 2px;
  -webkit-app-region: no-drag;
  
  svg {
    fill: #e7e7e7;
    transition: fill 100ms;
  }

`


const HeaderLabel = styled.p`
  font-size: 13px;
  color: #e7e7e7;
  margin: 0;
  margin-block-start: 0;
  margin-block-end: 0;
  line-height: 1;
  transition: color 100ms;

`

const HeaderItemWrapper = styled.div`
  height: 60px;
  width: 70px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 6px 0 4px 0;
  margin: 0 15px;
  transition: background-color 100ms;
  border-radius: 3px;
  cursor: pointer;

  :hover {
    background-color: #475462;

    svg {
      fill: #ffffff;
    }

    p {
      color: #ffffff;

    }
  }


`

const AppMenuWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #eaeaea;
  padding: 10px 10px;
  -webkit-app-region: no-drag;


`

const AppMenuItem = styled.div`
  padding: 8px 30px 8px 20px;

  :hover {
    background-color: #b2d9f1;
  }
`

const AppMenuText = styled.p`
  font-size: 16px;
  color: #383838;
  cursor: pointer;
  margin: 0;
`

const Divider = styled.div`
  width: 100%;
  height: 1px;
  background-color: #adadad;
  margin: 10px 0;
`

const OverlayWrapper = styled.div`
  height: calc(100vh - 100px);
  width: calc(100vw - 100px);
  background-color: white;
`

const AppMenu: React.FC<{handleClick: () => void}> = ({handleClick}) => {

    const handleMaximizeApp = () => {
        handleClick()
        window.ipcRender.send('window:maximize');

    }

    const handleMinimizeApp = () => {
        handleClick()
        window.ipcRender.send('window:minimize');

    }

    const handleRestoreApp = () => {
        handleClick()
        window.ipcRender.send('window:restore');

    }

    return (
        <AppMenuWrapper>
                <AppMenuItem onClick={handleMaximizeApp} >
                    <AppMenuText>Maximise</AppMenuText>
                </AppMenuItem>
            <AppMenuItem onClick={handleMinimizeApp}>
                <AppMenuText>Minimise</AppMenuText>
            </AppMenuItem>
            <AppMenuItem onClick={handleRestoreApp}>
                <AppMenuText>Restore</AppMenuText>
            </AppMenuItem>
            <Divider/>
            <AppMenuItem>
                <AppMenuText>Account Settings</AppMenuText>
            </AppMenuItem>
            <AppMenuItem>
                <AppMenuText>Application Settings</AppMenuText>
            </AppMenuItem>
            <Divider/>
            <AppMenuItem>
                <AppMenuText>Sign Out</AppMenuText>
            </AppMenuItem>
        </AppMenuWrapper>

    )
}

interface ElectronWindow extends Window {
    ipcRender: {
        send: (message: string) => void
    }

}



declare let window: ElectronWindow;

export const Header: React.FC<{ setBodyMask: Dispatch<boolean> }> = ({setBodyMask}) => {

    const [showAppMenu, setShowAppMenu] = useState<boolean>()

    const handleCloseApp = () => {
        window.ipcRender.send('window:close');
    }

    const handleToggleMaxMin = () => {
        window.ipcRender.send('window:toggle');
    }

    const handleToggleAppMenu = () => {
        setShowAppMenu(prev => !prev)
    }

    const handleAppMenuClick = () => {
        setShowAppMenu(false)
    }

    const handleAppMenuVisibleChange = (bool: boolean) => {
        setShowAppMenu(bool)
    }

    const handleOnOpen = (bool: boolean) => {
        setBodyMask(bool)
    }

    return (
        <HeaderWrapper>
            <MainBlock onDoubleClick={handleToggleMaxMin}>
                <HeaderItemBlockWrapper>
                    <DropdownWidget
                        overlayStyle={{zIndex: 10001}}
                        onVisibleChange={handleOnOpen}
                        trigger={['click']}
                        overlay={<ContactsDropdown/>} placement="bottomLeft" arrow={{ pointAtCenter: true }}>
                        <HeaderItemWrapper>
                            <IconWrapper height={26} top={3}>
                                <AddressBookIcon/>
                            </IconWrapper>
                            <HeaderLabel>Contacts</HeaderLabel>
                        </HeaderItemWrapper>
                    </DropdownWidget>
                </HeaderItemBlockWrapper>
                <HeaderItemBlockWrapper>
                    <HeaderItemWrapper>
                        <IconWrapper height={25} top={4}>
                            <CommentIcon/>
                        </IconWrapper>
                        <HeaderLabel>Messages</HeaderLabel>
                    </HeaderItemWrapper>
                    <HeaderItemWrapper>
                        <IconWrapper height={25} top={2}>
                            <CalendarSolidIcon/>
                        </IconWrapper>
                        <HeaderLabel>Schedule</HeaderLabel>
                    </HeaderItemWrapper>
                    <HeaderItemWrapper>
                        <IconWrapper height={26} top={3}>
                            <CheckboxIcon/>
                        </IconWrapper>
                        <HeaderLabel>Tasks</HeaderLabel>
                    </HeaderItemWrapper>
                </HeaderItemBlockWrapper>
            </MainBlock>
            <AppBlock>
                <CloseIconWrapper onClick={handleCloseApp}>
                    <XMarkIcon/>
                </CloseIconWrapper>
                <DropdownWidget placement="bottomRight" overlay={<AppMenu handleClick={handleAppMenuClick}/>} trigger={['click']} visible={showAppMenu} onVisibleChange={handleAppMenuVisibleChange }>
                    <MenuIconWrapper onClick={handleToggleAppMenu}>
                        <AngleArrowIcon/>
                    </MenuIconWrapper>
                </DropdownWidget>
            </AppBlock>
        </HeaderWrapper>
    )
}