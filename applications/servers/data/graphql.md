higher order resolvers

- node (abstract - node in a mesh/matrix thing)
- root
- join 
- set 

process query from a high level

pass on to resolvers 

joinType > joinInstance > models
can just hit the joinInstance tables

but then it is no longer a generic join.


globalJoins (node) vs schemaJoins (join)
global supertype

will require big look up on the globalJoin (node) table

joinTypes must be able to handle generic joins if they are to be useful

as in be able to provide a superType for multiple models <br>
would be great if the join would do its magic at a db level

an endpoint which returns a union type (one of multiple types)

sql query hits 'join', which resolves the union

so the join is defined in the schema with all the typing

can be 1:1 or M:1

this will be a little bit more tricky because it will also involve property decorators<br>
and involve a fair amount of typescript cleverness